let map = L.map("map", {
  zoomControl: true,
  maxZoom: 16,
  minZoom: 10,
  scrollWheelZoom: false,
}).setView([-30.0, -51.1611], 10);

map.zoomControl.setPosition("bottomright");

map.setMaxBounds(map.getBounds());

var imageUrl = "/wp-content/themes/jornal-sul21/donos-da-cidade/img/bgMapa.png",
  imageBounds = map.getBounds();
// imageUrl.style.width = "100vw";

L.imageOverlay(imageUrl, imageBounds).addTo(map);

L.geoJson(mzPoaData).addTo(map);

let tiles = L.tileLayer(
  "https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png",
  {
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
    subdomains: "abcd",
    maxZoom: 20,
  }
).addTo(map);

// funções relativas as MZ

// Cor das zonas

const colorsByZones = {
  MZ1: "#FFF392",
  MZ2: "#FFD4D2",
  MZ3: "#BCC4E3",
  MZ4: "#D4CA5F",
  MZ5: "#B9E5DE",
  MZ7: "#F3C66A",
  MZ8: "#D1F598",
  MZ9: "#80AF85",
  MZ10: "#FFB59E",
};

function getColor(z) {
  return colorsByZones.hasOwnProperty(z) ? colorsByZones[z] : "#80AF85";
}

// Design das MZ
let geojson = L.geoJson(mzPoaData, {
  style: function (feature) {
    return {
      fillColor: getColor(feature.properties.zone),
      weight: 2,
      opacity: 1,
      color: "white",
      dashArray: "0",
      fillOpacity: 1,
    };
  },
  onEachFeature: onEachFeature,
}).addTo(map);

// Destaque com hover
function highlightFeature(e) {
  var layer = e.target;

  layer.setStyle({
    weight: 5,
    color: "#666",
    dashArray: "",
    fillOpacity: 0.7,
  });

  layer.bringToFront();
}

function resetHighlight(e) {
  geojson.resetStyle(e.target);
}

// Estabelece zoom na MZ ao clicar
function zoomToFeature(e) {
  map.fitBounds(e.target.getBounds());
}

function onEachFeature(feature, layer) {
  layer.on({
    mouseover: highlightFeature,
    mouseout: resetHighlight,
    click: zoomToFeature,
  });
}

// Chave de visibilidade dos botões de navegação
let isHighlightsActive = false;

function toggleVisibility(selector, selectorSubMenu) {
  return function () {
    let menu = document.getElementById("nav-menu");
    let submenu = document.querySelector(selector);
    if (submenu.style.display === "none") {
      submenu.style.display = "block";
      menu.style.display = "none";
    } else {
      submenu.style.display = "none";
      menu.style.display = "block";
    }

    // Toggle the display of the legenda and listaLegenda elements
    const listaLegenda = document.querySelector("#listaLegendas");
    const allLegendas = document.querySelectorAll(".construtoraLegenda");
    allLegendas.forEach((legenda) => {
      legenda.style.display = "none";
    });
    if (listaLegenda.style.display === "none") {
      listaLegenda.style.display = "block";
    }

    //Toggle dos destaques na sessão destaques
    if (selector == ".menu-highlights") {
      if (isHighlightsActive) {
        isHighlightsActive = false;
        markersLayerGroup.clearLayers();
        allMarkers();
      } else {
        highlightsOnClick();
        isHighlightsActive = true;
        return;
      }
    }

    expandMenu(selectorSubMenu);

    markersLayerGroup.clearLayers();
    allMarkers();
  };
}

const toggleVisibilityHighlights = toggleVisibility(".menu-highlights");
const toggleVisibilityBuilders = toggleVisibility(
  ".menu-builders",
  ".construtoras"
);
const toggleVisibilityStage = toggleVisibility(".menu-stage", ".estagios");
const toggleVisibilityMacrozones = toggleVisibility(
  ".menu-macrozones",
  ".macrozonas"
);

// Gera os marcadores
let markersLayerGroup = new L.LayerGroup().addTo(map);

let marker;
allMarkers();
function allMarkers() {
  markers.forEach((item) => {
    marker = new L.marker([item[0], item[1]], {
      icon: item[3],
    }).bindPopup(item[2]);

    marker.on('popupopen', function () {
      // Centraliza o mapa na posição do marcador quando o popup é aberto
      map.panTo(marker.getLatLng());
    });

    markersLayerGroup.addLayer(marker);
  });
}

// Gera os marcadores por especificidade
function OnClick(value, index, selector, legendaParameters) {
  return function () {
    markersLayerGroup.clearLayers();

    const markersFiltrados = markers.filter((item) => item[index] === value);

    markersFiltrados.forEach((item) => {
      icon = new L.marker([item[0], item[1]], {
        icon: item[3],
      }).bindPopup(item[2]);

      marker.on('popupopen', function () {
        // Centraliza o mapa na posição do marcador quando o popup é aberto
        map.panTo(marker.getLatLng());
      });
      
      markersLayerGroup.addLayer(icon);
    });

    // Toggle the display of the legenda and listaLegenda elements
    let legenda = document.querySelector(legendaParameters);
    const listaLegenda = document.querySelector("#listaLegendas");

    const allLegendas = document.querySelectorAll(".construtoraLegenda");
    allLegendas.forEach((legenda) => {
      legenda.style.display = "none";
    });

    collapseMenu(selector);
  };
}

let melnickLegenda = document.querySelector("#melnickLegenda");
melnickLegenda.style.display = "none";
let cyrellaLegenda = document.querySelector("#cyrellaLegenda");
cyrellaLegenda.style.display = "none";
let zaffariLegenda = document.querySelector("#zaffariLegenda");
zaffariLegenda.style.display = "none";
let encorpLegenda = document.querySelector("#encorpLegenda");
encorpLegenda.style.display = "none";
let maiojamaLegenda = document.querySelector("#maiojamaLegenda");
maiojamaLegenda.style.display = "none";
let multiplanLegenda = document.querySelector("#multiplanLegenda");
multiplanLegenda.style.display = "none";
let tendaLegenda = document.querySelector("#tendaLegenda");
tendaLegenda.style.display = "none";
let mrvLegenda = document.querySelector("#mrvLegenda");
mrvLegenda.style.display = "none";
let lyxLegenda = document.querySelector("#lyxLegenda");
lyxLegenda.style.display = "none";
let listaLegendas = document.querySelector("#listaLegendas");

// Gera os marcadores POR CONSTRUTORA
const OnClickConstrutora = function (construtora, selector, legendaParameters) {
  return OnClick(construtora, 3, selector, legendaParameters);
};

const melnickOnClick = OnClickConstrutora(
  melnickIcon,
  ".construtoras",
  "#melnickLegenda"
);
const cyrellaOnClick = OnClickConstrutora(
  cyrellaIcon,
  ".construtoras",
  "#cyrellaLegenda"
);
const tendaOnClick = OnClickConstrutora(
  tendaIcon,
  ".construtoras",
  "#tendaLegenda"
);
const mrvOnClick = OnClickConstrutora(mrvIcon, ".construtoras", "#mrvLegenda");
const maiojamaOnClick = OnClickConstrutora(
  maiojamaIcon,
  ".construtoras",
  "#maiojamaLegenda"
);
const zaffariOnClick = OnClickConstrutora(
  zaffariIcon,
  ".construtoras",
  "#zaffariLegenda"
);
const lyxOnClick = OnClickConstrutora(lyxIcon, ".construtoras", "#lyxLegenda");
const encorpOnClick = OnClickConstrutora(
  encorpIcon,
  ".construtoras",
  "#encorpLegenda"
);
const othersOnClick = OnClickConstrutora(
  outrasIcon,
  ".construtoras",
  "#multiplanLegenda"
);
const highlightsOnClick = OnClickConstrutora(
  destacarIcon,
  ".construtoras",
  "#listaLegendas"
);

// Gera os marcadores POR ZONA

const OnClickZona = function (zona, selector) {
  return OnClick(zona, 4, selector);
};

const MZ1OnClick = OnClickZona("MZ1", ".macrozonas");
const MZ2OnClick = OnClickZona("MZ2", ".macrozonas");
const MZ3OnClick = OnClickZona("MZ3", ".macrozonas");
const MZ4OnClick = OnClickZona("MZ4", ".macrozonas");
const MZ5OnClick = OnClickZona("MZ5", ".macrozonas");
const MZ6OnClick = OnClickZona("MZ6", ".macrozonas");
const MZ7OnClick = OnClickZona("MZ7", ".macrozonas");
const MZ8OnClick = OnClickZona("MZ8", ".macrozonas");
const MZ9OnClick = OnClickZona("MZ9", ".macrozonas");
const MZ10OnClick = OnClickZona("MZ10", ".macrozonas");

// Gera os marcadores POR ESTÁGIO DA OBRA

const OnClickEstagio = function (estagio, selector) {
  return OnClick(estagio, 5, selector);
};

const inProgressOnClick = OnClickEstagio("Em andamento", ".estagios");
const designedOnClick = OnClickEstagio("Projetado", ".estagios");
const concludedOnClick = OnClickEstagio("Concluido", ".estagios");

//Função para aparecer a navegação

const mediaQuerySmall = window.matchMedia("(min-height: 200px)");
const mediaQueryMedium = window.matchMedia("(min-height: 500px)");
const mediaQueryMediumLarge1 = window.matchMedia("(min-height: 700px)");
const mediaQueryMediumLarge2 = window.matchMedia("(min-height: 900px)");
const mediaQueryLarge = window.matchMedia("(min-height: 1024px)");

var nav = document.querySelector("#container-nav");
nav.style.visibility = "hidden";
console.log(window.scrollY);
window.addEventListener("scroll", () => {
  if (mediaQueryLarge.matches) {
    if (window.scrollY >= 900 && window.scrollY < 1700) {
      console.log(window.scrollY);

      nav.style.visibility = "visible";
    } else {
      nav.style.visibility = "hidden";
    }
  } else if (mediaQueryMediumLarge1.matches) {
    if (window.scrollY >= 650 && window.scrollY < 1350) {
      console.log(window.scrollY);

      nav.style.visibility = "visible";
    } else {
      nav.style.visibility = "hidden";
    }
  } else if (mediaQueryMediumLarge2.matches) {
    if (window.scrollY >= 700 && window.scrollY < 1450) {
      console.log(window.scrollY);

      nav.style.visibility = "visible";
    } else {
      nav.style.visibility = "hidden";
    }
  } else if (mediaQueryMedium.matches) {
    if (window.scrollY >= 600 && window.scrollY < 1100) {
      console.log(window.scrollY);

      nav.style.visibility = "visible";
    } else {
      nav.style.visibility = "hidden";
    }
  } else if (mediaQuerySmall.matches) {
    if (window.scrollY >= 500  && window.scrollY < 900) {
      console.log(window.scrollY);

      nav.style.visibility = "visible";
    } else {
      nav.style.visibility = "hidden";
    }
  }
});

//Button go top
let goTopbutton = document.querySelector("#goTopBtn");
goTopbutton.style.display = "none";
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if (
    document.body.scrollTop > 500 ||
    document.documentElement.scrollTop > 500
  ) {
    goTopbutton.style.display = "block";
  } else {
    goTopbutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function goTop() {
  window.scrollTo({
    top: 0,
    behavior: "smooth",
  });
}

let expandButtonBuilders = document.querySelector("#expandButtonBuilders");
let expandButtonStage = document.querySelector("#expandButtonStage");
let expandButtonMZ = document.querySelector("#expandButtonMZ");
let divExpandButton = document.querySelector(".divExpandButton");
divExpandButton.style.display = "none";

//Add toggle to navmenu
function collapseMenu(selector) {
  let submenu = document.querySelector(selector);

  submenu.style.display = "none";

  if (selector == ".construtoras") {
    expandButtonBuilders.style.display = "block";
    divExpandButton.style.display = "flex";
  } else if (selector == ".estagios") {
    expandButtonStage.style.display = "block";
    divExpandButton.style.display = "flex";
  } else if (selector == ".macrozonas") {
    expandButtonMZ.style.display = "block";
    divExpandButton.style.display = "flex";
  }
}

function expandMenu(selector) {
  let submenu = document.querySelector(selector);

  return function () {
    submenu.style.display = "flex";

    if (selector == ".macrozonas") {
      expandButtonMZ.style.display = "none";
      divExpandButton.style.display = "none";
    } else if (selector == ".construtoras") {
      expandButtonBuilders.style.display = "none";
      divExpandButton.style.display = "none";
    } else if (selector == ".estagios") {
      expandButtonStage.style.display = "none";
      divExpandButton.style.display = "none";
    }
  };
}

// const expandMenuHighlights = expandMenu(".destaques");
const expandMenuBuilders = expandMenu(".construtoras");
const expandMenuStage = expandMenu(".estagios");
const expandMenuMacrozones = expandMenu(".macrozonas");
