# Especial 'Donos da Cidade' para o Jornal Sul 21

Repo https://gitlab.com/coletivofarpa/donos-da-cidade

## Como usar o projeto em desenvolvimento:

Os comandos abaixo são executados a partir do diretório raiz do projeto.

Dependências:

- Novagador Firefox, entre outros
- Editor de código/texto puro ou IDE desde que seja Software Livre ;) 

Para rodar um servidor local com o projeto e o Sass:

Distros GNU com ou sem Linux
``` Bash
sass --watch scss/estilo.scss:css/estilo.css --style compressed
```
rWindowns
``` Bash
sass --watch scss/estilo.scss css/estilo.css --style compressed
```
Abra o arquivo index.html no navegador Firefox, entre outros.
