<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="Um jornal independente comprometido com a defesa da diversidade, dos direitos, do meio ambiente e da democracia." name="description">
    <meta content="Um jornal independente comprometido com a defesa da diversidade, dos direitos, do meio ambiente e da democracia." property="og:description">
    <meta content="Um jornal independente comprometido com a defesa da diversidade, dos direitos, do meio ambiente e da democracia." property="twitter:description">
    <meta content="Um jornal independente comprometido com a defesa da diversidade, dos direitos, do meio ambiente e da democracia." name="abstract">
    <meta content name="keywords">
    <meta content="Donos da Cidade - Especial Sul 21" property="og:title">
    <meta content="Donos da Cidade - Especial Sul 21" property="twitter:title">
    <meta name="robots" content="index, follow">
    <meta property="og:type" content="website">
    <meta content="summary" name="twitter:card">
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Donos da Cidade - Especial Sul 21" />
    <meta property="og:url" content="<?php echo get_site_url(); ?>/donos-da-cidade/" />
    <meta property="og:site_name" content="Sul 21" />
    <meta property="og:image" content="<?php echo get_site_url(); ?>/wp-content/uploads/2023/10/donos-da-cidade-dest-min.png" />
    <meta property="og:image:width" content="1000" />
    <meta property="og:image:height" content="1000" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@sulvinteum" />
    <title>Donos da Cidade - Especial Sul 21</title>
    <!-- Incluindo a lib CSS Reset -->
    <link rel="stylesheet" href="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/css/reset.css" />

    <!-- Font Awesome -->
    <link
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
      rel="stylesheet"
    />
    <!-- Google Fonts -->
    <link
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
      rel="stylesheet"
    />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.4.1/mdb.min.css" rel="stylesheet" />

    <!-- Inserindo família de fontes tipográficas -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600&display=swap"
      rel="stylesheet"
    />

    <!-- Auto hospedando o arquivo CSS da lib Leaflet na seção de cabeçalho <head> do documento html -->
    <!-- <link rel="stylesheet" href="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/leaflet/leaflet.css"> -->
    <link
      rel="stylesheet"
      href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
      integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI="
      crossorigin=""
    />

    <!-- Auto hospedando o arquivo Leaflet JavaScript após o CSS da Leaflet 
    <script src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/leaflet/leaflet.js"></script>
    -->
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script
      src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
      integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM="
      crossorigin=""
    ></script>

    <!-- Adicionando kit de ferramentas de frontend Bootstrap -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous"
    />

    <!-- Incluindo nossa folha de estilos CSS -->
    <link rel="stylesheet" href="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/css/estilo.css">
    <style>
     #cases .img-fluid {
       width: 250px;
       height: 140px;
     }
   </style>
  </head>

  <body class="overflow-x-hidden">
    <header class="fixed-top">
      <!-- Header -->
      <div class="header">
        <div class="container">
          <section class="d-flex align-items-center justify-content-between">

            <div class="col-md-5 d-flex align-items-start justify-content-start mb-md-0 text-dark text-decoration-none">
              <a href="<?php echo get_site_url(); ?>">
                <img id="sul21" class="img-fluid" width="70" height="33" src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/logo-Sul21-preta-70x33.svg" alt="">
              </a>
            </div>

            <div class="col-md-2 d-flex align-items-center justify-content-center text-center">
              <a href="#">
                <img class="logo" width="100" height="77" src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/donos-da-cidade-logo-100x77.svg" alt="Especial Donos da Cidade logotipo" />
              </a>
            </div>

            <div class="menu-marcador col-md-5 d-flex align-items-end justify-content-end">
              <!-- Menu mobile Humburguer offcanvas -->
              <!-- Ícone hamburguer animado -->
              <div class="menu-icon d-lg-none" id="menu-icon">
                <div class="bar"></div>
                <div class="bar"></div>
                <div class="bar"></div>
              </div>
              <!-- /FIM do Ícone hamburguer animado -->
              
              <div id="offcanvasNavbar" class="offcanvas offcanvas-end" tabindex="-1" aria-labelledby="offcanvasNavbarLabel">
                <!-- Conteúdo do menu offcanvas aqui -->
                <nav class="">
                  <!-- Conteúdo de navegação aqui -->
                  <a href="#" class="nav-link px-2" onclick="closeOffcanvas()">Home</a>
                  <a href="#mapa" class="nav-link px-2" onclick="closeOffcanvas()">Mapa</a>
                  <a href="#reportagens" class="nav-link px-2" onclick="closeOffcanvas()">Reportagens</a>
                  <a href="#cases" class="nav-link px-2" onclick="closeOffcanvas()">Destaques</a>
                  <a href="#creditos" class="nav-link px-2" onclick="closeOffcanvas()">Créditos</a>
                </nav>
              </div>
              <!-- /FIM do Menu mobile Humburguer offcanvas -->
              

              <!-- Menu Desktop -->
              <ul id="sul21" class="nav mb-md-0">
                <li><a href="#" class="nav-link px-2 link-dark">Home</a></li>
                <li><a href="#mapa" class="nav-link px-2 link-dark">Mapa</a></li>
                <li><a href="#reportagens" class="nav-link px-2 link-dark">Reportagens</a></li>
                <li><a href="#cases" class="nav-link px-2 link-dark">Destaques</a></li>
                <li><a href="#creditos" class="nav-link px-2 link-dark">Créditos</a></li>
              </ul>
              
            </div>
            
          </section>
        </div>
      </div>

      <!-- Menus -->
      <nav id="container-nav" class="container">
        <!-- Menu -->
        <div class="menu text-white" id="nav-menu">
          <ul class="row gx-0">
            <li class="destaque col" onclick="toggleVisibilityHighlights()">
              <div class="nav-icone">
                <img
                  src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/destaque-icone.svg"
                  alt="Botão Projetos em Destaque"
                />
                <p>Projetos em Destaque</p>
              </div>
            </li>
            <li class="builders col" onclick="toggleVisibilityBuilders()">
              <img
                src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/construtoras-icone.svg"
                alt="Botão Construtoras"
              />
              <p>Construtoras</p>
            </li>
            <li class="stage col" onclick="toggleVisibilityStage()">
              <div class="nav-icone">
                <img
                  src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/obra-icone.svg"
                  alt="Botão Estágio da Obra"
                />
                <p>Estágio da Obra</p>
              </div>
            </li>
            <li class="macrozones col" onclick="toggleVisibilityMacrozones()">
              <div class="nav-icone">
                <img
                  src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/macrozonas-icone.svg"
                  alt="Botão Macrozonas"
                />
                <p>Macrozonas</p>
              </div>
            </li>
          </ul>
        </div>

        <!-- Submenus -->
        <div class="submenu">
          <!-- Destaques -->
          <div class="menu-highlights" style="display: none">
            <div class="submenu-destaques row justify-content-center">
              <div
                class="seta-voltar col-1"
                onclick="toggleVisibilityHighlights()"
              >
                <img src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/seta-voltar.svg" alt="" />
              </div>
              <p class="col-10">Projetos em Destaque</p>
              <div class="submenu-icone col-1">
                <img
                  src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/destaque-icone.svg"
                  alt="Botão Projetos em Destaque"
                />
              </div>
            </div>

            <ul class="destaques row">
              <li class="destaque-1 col-4">
                <!-- Botão para abrir a modal 1 -->
                <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal1">
                  Arena do Grêmio
                </button>

                <!-- Modal -->
                <div class="modal" data-bs-backdrop="false" id="modal1" tabindex="-1" aria-labelledby="modal1Label" aria-hidden="true">
                  <div class="modal-dialog modal-lg"> <!-- Use a classe modal-lg para uma modal grande -->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="modal1Label">
                          Após uma década da inauguração do estádio do Grêmio, o Humaitá amarga a promessa de revitalização que viria com o bairro planejado pela empreiteira OAS
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                          FECHAR
                        </button>
                      </div>
                      <div class="modal-body">
                        <span>
                          <em>O Complexo Multiuso não se concretizou e cinco das sete torres residenciais construídas ao lado da Arena ainda não receberam o habite-se da prefeitura.</em>
                            <br>
                          <br>Em dezembro de 2022, a 10ª Vara da Fazenda Pública de Porto Alegre determinou que a Arena Porto-Alegrense, a Albizia Empreendimentos e a Karagounis Participações (as duas últimas proprietárias das torres de apartamentos ao lado da Arena do Grêmio) pagassem ao município R$ 193 milhões para custear as obras não executadas no bairro Humaitá. As empresas recorreram da decisão contestando o valor apurado pelo Tribunal de Contas do Estado, o pedido foi acatado pelo Tribunal de Justiça e, em fevereiro deste ano, foi fixado novo valor provisório de pouco mais de R$ 44 milhões. 
                          <br><br>A sentença também caiu por terra porque o acordo firmado entre os empreendedores e município em 2021 estava vinculado à compra da Arena pelo Grêmio, o que não aconteceu. A justiça, então, desobrigou as empresas de realizarem as obras e as obrigações voltaram para a OAS S.A, conforme contrato assinado em 2014. A empreiteira até hoje não executou boa parte das medidas de acessibilidade viária, esgotamento sanitário e pluvial da região, acordadas há mais de dez anos quando o empreendimento foi licenciado pela Prefeitura. Desde que entrou com pedido de recuperação judicial, em 2015, a OAS vem acumulando justificativas para não fazer o que se comprometeu. 

                            <br><br>A situação frustra quem comprou um imóvel esperando que a região valorizasse, mas também quem nunca conseguiu acessar a nova moradia. As torres 3, 4, 5, 6 e 7 dos condomínios residenciais Gran Vista e Bella Vista, que são parte do Complexo Imobiliário Liberdade, não receberam a carta de habitação da prefeitura. Sem o documento, os imóveis não podem receber moradores. 

                            <br><br>Distribuídos em sete torres, todos os 930 imóveis no bairro Liberdade foram vendidos na fase de lançamento, mas apenas dois prédios receberam o habite-se. Como a promessa de melhorias no entorno foi descumprida, os compradores de um apartamento no Condomínio Residencial Liberdade, <a href="<?php echo get_site_url(); ?>/cidades/2018/08/empresa-e-condenada-a-indenizar-compradores-de-condominio-isolado-no-entorno-da-arena/" target="_blank">obtiveram na Justiça direito ao cancelamento</a> do contrato de compra da unidade.

                            <br><br>“Vendido” ao município como um projeto que iria renovar e redefinir o perfil de desenvolvimento de toda a zona norte de Porto Alegre, o Complexo Multiuso que seria construído no entorno da Arena do Grêmio não saiu do papel e ainda trouxe uma série de problemas à região. Em 2010, a construtora declarou que os <a href="https://www1.folha.uol.com.br/fsp/esporte/fk2503201014.htm" target="_blank">investimentos em todo o complexo</a> ficariam em torno de R$ 1 bilhão, só a Arena custaria R$ 400 milhões. O projeto ainda previa um shopping, centro de convenções, hotel e edifícios residenciais, além de 8.480 vagas de estacionamento.

                            <br><br>A área de 38 mil metros quadrados às margens da BR-290, onde está localizado o estádio, havia sido doada à Federação dos Círculos Operários do Rio Grande do Sul (FCORS), pelo Governo do Estado, com a condição de <a href="https://www.al.rs.gov.br/FileRepository/repLegisComp/Lei%20n%C2%BA%2004.610.pdf" target="_blank">não ser vendida nem penhorada</a>. Para autorizar a negociação da OAS com a instituição,  a ex-governadora Yeda Crusius <a href="http://proweb.procergs.com.br/Diario/DA20081022-01-100000/EX20081022-01-100000-PL-242-2008.pdf" target="_blank">enviou um projeto de lei</a> em regime de urgência à Assembleia Legislativa do Estado para mudar a Lei 4.610/1963 e dar seguimento no empreendimento. Outro Projeto de Lei isentou o clube dos tributos fiscais estaduais. “<a href="https://estado.rs.gov.br/yeda-apoia-projeto-do-estadio-arena-fifa-do-gremio" target="_blank">É um projeto para o desenvolvimento do Estado</a>”, disse Yeda em 2007. 

                            <br><br>No ano seguinte, em sessão especial dirigida pelo presidente da Câmara Municipal, o ex-vereador Sebastião Melo (PMDB), o projeto para o complexo esportivo foi apresentado na casa legislativa com pedido de apoio aos parlamentares. O “estádio mais moderno do mundo”, nas palavras de Paulo Odone, presidente do clube à época, "merece máxima atenção", complementou Melo. O dirigente queria <a href="https://www.camarapoa.rs.gov.br/noticias/gremio-apresenta-projeto-da-arena-multiuso-aos-vereadores" target="_blank">mudar o regime urbanístico</a> do terreno para poder construir a Arena. Alguns meses depois, a Câmara de Vereadores aprovou a <a href="https://leismunicipais.com.br/a/rs/p/porto-alegre/lei-complementar/2009/61/610/lei-complementar-n-610-2009-define-regime-urbanistico-para-subunidade-2-da-unidade-de-estruturacao-urbana-ueu-8-da-macrozona-mz-2-e-para-a-subunidade-3-da-ueu-80-da-mz-1-suprime-a-area-especial-de-interesse-institucional-da-subunidade-3-da-ueu-80-da-mz-1-e-da-outras-providencias" target="_blank">Lei Complementar 610/2009</a>.

                            <br><br>Diante da dimensão do empreendimento, o Termo de Compromisso assinado entre a empresa e o município gerou uma série de obrigações que deveriam ser cumpridas a fim de neutralizar os impactos negativos provocados pela instalação do projeto arquitetônico e a chegada de milhares de novos moradores na região, ainda hoje carente de infraestrutura. Foram então acordadas 30 medidas de mobilidade e sanitárias que melhorariam o entorno e o acesso ao estádio, além de obras sociais decorrentes das medidas mitigadoras e compensatórias que o empreendedor deveria entregar à sociedade. 

                            <br><br>Mesmo com todas as flexibilizações concedidas, a Arena foi inaugurada sem que a empresa tivesse cumprido boa parte das obras. Na festa de abertura do novo estádio, a direção do Grêmio e a engenharia de trânsito emitiram nota orientando o torcedor a não ir de carro na festa de inauguração, porque o complexo viário da região não tinha sido concluído. Em 2023, o bairro ainda sofre com alagamentos em dias de chuva e a falta de saneamento básico, com esgoto correndo a céu aberto. 

                            <br><br>Nas tentativas de acordos firmados nos últimos anos, o poder municipal desonerou a OAS de boa parte de suas obrigações. Houve redução de 30% no volume de obras. Mas isso também não foi suficiente para a empresa cumprir com o acertado. De acordo com a Procuradoria-Geral do Município – responsável por monitorar a entrega dessas ações –, alguns serviços pactuados há dois anos em acordo judicial foram executados parcialmente. Outros, relacionados ao sistema de drenagem da região ainda estão sendo contratados pelo Departamento Municipal de Água e Esgotos com recursos da Arena Porto-alegrense. 

                            <br><br>Entre as melhorias previstas no Estudo de Viabilidade Urbanística e Estudo de Impacto Ambiental estavam, por exemplo, a duplicação da avenida A. J. Renner e da Avenida Voluntários da Pátria, a conclusão da duplicação da Avenida Padre Leopoldo Brentano, uma alça para ligar a avenida Ernesto Neugebauer à freeway e ciclovia nas avenidas Voluntários da Pátria e Leopoldo Brentano. 
                            <br><br><strong>Obras viárias NÃO executadas pela OAS</strong>
                            <ul>
                              <li>● Duplicação da avenida A. J. Renner </li>
                              <li>● Duplicação da avenida Voluntários da Pátria</li>
                              <li>● Execução e reformulação da interseção da Avenida A. J. Renner com a Avenida Padre Leopoldo Brentano</li>
                              <li>● Execução da interseção da Avenida A. J. Renner com a Rua Dona Teodora</li>
                              <li>● Conclusão da execução da Avenida Padre Leopoldo Brentano</li>
                              <li>● Execução do trecho III da Avenida Voluntários da Pátria</li>
                              <li>● Execução da Rua 02 entre a diretriz 2122 (prolongamento da Av. A. J. Renner) e a Rua 01 (contorno da Arena do Grêmio)</li>
                              <li>● Construção de Estação de Bombeamento de Esgoto para atender os bairros Farrapos e Humaitá</li>
                              <li>● Implantação do terminal de ônibus junto a A. J. Renner com a Padre Leopoldo Brentano</li>
                            </ul>
                            <br><strong>Obras sociais NÃO executadas pela OAS</strong>
                            <ul>
                              <li>● Construção do Centro Cultural, localizado na Rua Frederico Mentz</li>
                              <li>● Reforma e Ampliação da ACEBERGS - Associação das Creches Beneficentes do Rio Grande do Sul</li>
                              <li>● Reforma e Ampliação da ASCOMAQ - Associação Beneficente Comunitária do Conjunto Residencial Mário Quintana (que mantém creche comunitária)</li>
                              <li>● Reforma e Ampliação da Construção da Sede da Associação localizada na antiga Área do SESI, na rua Frederico Mentz (executada parcialmente)</li>
                              <li>● Construção de Unidade de Triagem com pavilhão para separação e reciclagem conexa ao Centro de Triagem localizado na Frederico Mentz</li>
                              <li>● Realocação de Posto da brigada Militar (11º BPM)</li>
                              <li>● Estande de venda na Arena para divulgação e comercialização de produtos produzidos por entidades sociais da região em dias de jogos e dias de semana.</li>
                            </ul>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="destaque-2 col-4">
              <!-- Botão para abrir a modal 2 -->
                <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal2">
                  Botanique
                </button>

              <!-- Modal -->
                <div class="modal" data-bs-backdrop="false" id="modal2" tabindex="-1" aria-labelledby="modal2Label" aria-hidden="true">
                  <div class="modal-dialog modal-lg"> <!-- Use a classe modal-lg para uma modal grande -->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="modal2Label">
                          Dois mil metros quadrados de Mata Atlântica se transformam em jardim de condomínio privado no bairro Petrópolis, em Porto Alegre</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                          FECHAR
                        </button>
                      </div>
                      <div class="modal-body">
                        <span>
                          <em>Um córrego, árvores nativas e exóticas, espécies ameaçadas de extinção no Rio Grande do Sul compõe o entorno da passarela que será utilizada pelos moradores
                          </em>
                          <br><br>Em frente ao futuro Complexo Belvedere, no bairro Petrópolis, um terreno ao lado do Jardim Botânico foi preparado para receber o condomínio Botanique Résidence, parceria da incorporadora Melnick com a Guatambu Incorporações. O anúncio de venda dos apartamentos avisa: “Você está no pulmão de Porto Alegre”. Em consulta ao zoneamento e regime urbanístico da área, no site da Secretaria de Meio Ambiente, Urbanismo e Sustentabilidade, é possível verificar a faixa de quase dois mil metros quadrados de vegetação que foi preservada. 

                          <br><br>Mudar para: "No local, entre árvores nativas e exóticas, há uma paineira (Ceiba speciosa) – que está na lista oficial das espécies ameaçadas de extinção no Rio Grande do Sul, – um butiazeiro, duas figueiras do gênero ficus, essas últimas com corte proibido em todo o Estado. Bioma Mata Atlântica e um córrego também fazem parte da Área de Preservação Permanente. 
                          
                          <br><br>Embora o empreendedor declare em documentação que “não haverá intervenção na APP”, o projeto arquitetônico prevê um deck-passarela para caminhadas e “sala de estar” com bancos dentro da mata. Em seu parecer, a New Engenharia – empresa que assina o laudo de cobertura vegetal – atesta “não haver empecilhos para a instalação do empreendimento, pois o proprietário irá manter a área limpa e cercada”. 
                          
                          <br><br>Em 2014, o licenciamento do projeto foi condicionado à assinatura de Termo de Compromisso que apontou as obrigações que deveriam ser atendidas para mitigar e compensar os impactos decorrentes da obra. Alguns anos depois, em 2021, a incorporadora Melnick adquire parte do imóvel, negociado em permuta com a Guatambu, e em maio do mesmo ano, a empresa assume o pagamento da licença ambiental e um novo termo de compromisso é assinado com a Secretaria de Meio Ambiente, Urbanismo e Sustentabilidade. 
                          
                          <br><br>No documento, o município determina que a compensação ambiental referente ao plantio de 2084 mudas de árvores nativas seja feita em dinheiro. O valor foi fixado em 41.680 unidades financeiras municipais, o equivalente a R$ 185 mil à época, que deveria ser destinado ao Fundo Pró-defesa do Meio Ambiente.
                          
                          <br><br>Além da obrigação de atender a legislação ambiental, o empreendedor também deveria entregar ao município obras de drenagem, sinalização viária, equipamentos e instalações para a Central de Controle e Monitoramento da EPTC. O acordo prevê multas para o caso de eventual descumprimento. No entanto, segundo a Procuradoria-Geral do Município, responsável por monitorar a entrega dos compromissos, nenhuma das obrigações foi entregue até o momento, mas “são condicionantes para a entrega do habite-se”.
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </li>

              <li class="destaque-3 col-4">
              <!-- Botão para abrir a modal 3 -->
                <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal3">
                  Cassol Centerlar
                </button>

              <!-- Modal -->
                <div class="modal" data-bs-backdrop="false" id="modal3" tabindex="-1" aria-labelledby="modal3Label" aria-hidden="true">
                  <div class="modal-dialog modal-lg"> <!-- Use a classe modal-lg para uma modal grande -->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="modal3Label">
                          Suspeita de contaminação da água subterrânea e saídas de emergência inadequadas não impediram funcionamento do shopping Porto Alegre Centerlar</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                          FECHAR
                        </button>
                      </div>
                      <div class="modal-body">
                        <span>
                          <em>Procuradoria-Geral do Município afirma que as pendências foram resolvidas pelo empreendedor, mas não forneceu as comprovações
                          </em>
                          <br><br>Com a primeira fase concluída ainda em 2009, o shopping Porto Alegre Centerlar, construído pelos grupos Cassol e Zaffari, ocupa um terreno de 33,7 mil metros quadrados na esquina das avenidas Sertório e Assis Brasil, zona norte da capital. O projeto inicial era composto por loja de material de construção, hipermercado, um conjunto de 120 lojas, além de 1200 vagas de estacionamento, mas apenas parte do prédio foi liberado para a inauguração com Carta de Habitação e Alvará de Funcionamento em caráter provisório. 

                          <br><br>De acordo com as informações do Termo de Compromisso – contrato de obrigações assinado entre empreendedor e prefeitura –, estavam autorizadas para funcionamento as lojas-âncoras Cassol Centerlar e hipermercado Zaffari, “todas as demais áreas deverão ficar sem utilização tendo em vista que correspondem a áreas não aprovadas ou executadas em parte ou totalmente em desacordo com a legislação" apontava o documento de vistoria emitido à época. 
                          
                          <br><br>Embora a empresa tenha destacado em seu site que a obra foi realizada com “instalações e equipamentos desenvolvidos a partir de critérios de eficiência e sustentabilidade ambiental”, o empreendimento abriu as portas para o público com uma lista extensa de problemas e restrições apontados pelos dos órgãos municipais. Em 2014, cinco anos após a inauguração, um segundo acordo foi firmado em Termo de Compromisso Aditivo com a Cassol Materiais de Construção na tentativa de solucionar as pendências que o empreendedor tinha com o município. 
                          
                          <br><br>Entre as ações urgentes indicadas pela extinta Secretaria Municipal de Meio Ambiente, estava a elaboração de Laudo Técnico atestando que os sistemas de armazenamento aéreo de combustíveis do empreendimento estavam de acordo com as normas exigidas, após suspeita de contaminação das águas subterrâneas e, em caso de confirmação, exigia o detalhamento do risco toxicológico para a saúde humana e providências. 
                          
                          <br><br>Além do diagnóstico ambiental de solo e água, a empresa também deveria atualizar o Alvará de Prevenção da Proteção Contra Incêndio que estava vencido e cumprir outros compromissos assumidos, como correção imediata das saídas de emergência, implantar projeto de arborização, sinalização viária, atender ao Decreto Municipal 9.325/88 em relação a emissão de poluentes atmosféricos, entre outros. 
                          
                          <br><br>O documento informava também que a execução do empreendimento estava submetida ao cumprimento das condições e restrições estabelecidas na Licença de Instalação, sob pena de novo indeferimento do habite-se, além de prever multa diária no valor de R$ 5 mil em caso de descumprimento. 
                          <br><br>Mais de uma década depois da inauguração do shopping, não é possível saber se as irregularidades foram totalmente corrigidas. Procurada pela reportagem, a empresa informou por meio de nota que o empreendimento passou por diversas vistorias e recebeu, em 20/11/2015, o habite-se para a área total edificada. “Para emissão da Carta de Habitação, houve a devida verificação do atendimento dos itens previstos nos termos junto às secretarias e departamentos municipais. Entre estes itens, destaca-se a apresentação do Alvará do Plano de Prevenção de Combate a Incêndio do Corpo de Bombeiros, que, na sequência, permitiu a expedição da Licença de Operação, que vem sendo renovada. As melhorias viárias, conforme se verifica no entorno, também foram implantadas”. No entanto, não se pronunciou sobre as pendências que exigiam diagnóstico ambiental de solo e água.
                          <a href="https://docs.google.com/document/d/19nkHCc46_UUflInK8MO2EvD-E2r8nGW9V21-9nwMySE/edit" target="_blank"> Leia a nota na íntegra. </a>
                          
                          <br><br>A Procuradoria-Geral do Município, responsável pelo monitoramento da entrega das obrigações, não forneceu as comprovações, mas informou que “os compromissos firmados no TC foram quitados”, e que as questões ambientais deveriam ser solicitadas à Secretaria Municipal de Meio Ambiente, Urbanismo e Sustentabilidade, que, por sua vez, não retornou ao pedido de informação até o fechamento desta publicação, mas o espaço segue aberto. 
                          
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="destaque-4 col-4">
              <!-- Botão para abrir a modal 4 -->
                <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal4">
                  Central Parque
                </button>

              <!-- Modal -->
                <div class="modal" data-bs-backdrop="false" id="modal4" tabindex="-1" aria-labelledby="modal4Label" aria-hidden="true">
                  <div class="modal-dialog modal-lg"> <!-- Use a classe modal-lg para uma modal grande -->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="modal4Label">
                          Construtora Rossi construiu o primeiro bairro privado de Porto Alegre, e mesmo com os benefícios que recebeu da prefeitura não entregou as obras de melhoria para o bairro</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                          FECHAR
                        </button>
                      </div>
                      <div class="modal-body">
                        <span>
                          <em>O Central Parque, no Jardim do Salso, ocupa uma área de 200 mil metros quadrados na zona norte da capital</em>
                          <br><br>O Central Parque, planejado pela Rossi, é o primeiro bairro privado de Porto Alegre. Lançado em 2012, os quatro condomínios construídos pela construtora se estendem por duas quadras ao norte da rua São Josemaría Escrivá e às margens da José Albano Volkmer. Rossi Parque Ibirapuera, Rossi Parque Panamby e Rossi Arte Parque, concluídos em 2013; Rossi Estilo e Rossi Business Park, concluídos em 2017; E Rossi Reserva e Supreme Central Parque (da construtora Melnick), concluídos em 2019.

                          <br><br>Localizado no bairro Jardim do Salso, o bairro privativo tem 220 mil metros quadrados, e é formado por quarteirões comerciais e residenciais. Ao longo dos últimos anos, parte dos loteamentos foram sendo vendidos a outras construtoras. Ao assinar o Termo de Compromisso com a prefeitura para a construção do empreendimento, a empresa se comprometeu com uma série de obrigações de mitigação e compensação de impacto decorrentes da implantação do bairro, mas inúmeros aditivos no contrato foram repactuando as obras a medida que não eram entregues. As Cartas de Habite-se das quadras do empreendimento, inicialmente atreladas a entrega das obras, acabou sendo desvinculada do término total das obras de infraestrutura do projeto.
                          
                          <br><br>No ano passado, Rossi entrou com pedido de recuperação judicial, deixando para trás boa parte dos compromissos por fazer, como a reconstrução da Escola Estadual de Ensino Fundamental Professora Lea Rosa Cecchini, que nunca foi feita. A Prefeitura, à época, removeu 56 famílias para permitir a construção de uma praça e de uma rua, além de uma bacia de detenção pluvial, para evitar alagamentos na região. As obras eram parte das obrigações do empreendedor com o município. 
                          
                          <br><br>A entrega da praça está sendo cobrada judicialmente pelo Ministério Público até hoje. Como não havia conseguido conciliação com os empreendedores, o órgão hipotecou o único imóvel que a Rossi tinha em seu nome no Rio Grande do Sul, encontrado na cidade de Pelotas, zona sul do Estado, até que a empresa pague o que deve ao Município. A única obrigação cumprida pela construtora foi o Centro Cultural e Esportivo Bom Jesus, aberto à comunidade no final de 2012.
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="destaque-5 col-4">
                <!-- Botão para abrir a modal 5 -->
                <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal5">
                  Complexo Belvedere
                </button>

              <!-- Modal -->
                <div class="modal" data-bs-backdrop="false" id="modal5" tabindex="-1" aria-labelledby="modal5Label" aria-hidden="true">
                  <div class="modal-dialog modal-lg"> <!-- Use a classe modal-lg para uma modal grande -->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="modal5Label">
                          Complexo Belvedere toma forma em meio a Área de Preservação Permanente</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                          FECHAR
                        </button>
                      </div>
                      <div class="modal-body">
                        <span>
                          <em>Entre as áreas especiais de interesse ambiental do município estão vegetação de Mata Atlântica, fauna silvestre e fonte de água mineral</em>
                          <br><br>Quem passa pela avenida Senador Tarso Dutra, na altura do número 500, já consegue visualizar o “vazio” produzido pela remoção da vegetação que dará lugar ao futuro Complexo Belvedere, na zona norte de Porto Alegre. A área verde, ao lado do Jardim Botânico, será substituída por um shopping, um hipermercado Zaffari e duas torres comerciais de acordo com o projeto aprovado na Prefeitura. Idealizado ainda em 1995, o empreendimento precisou passar por inúmeras modificações por estar localizado em Área de Preservação Permanente (APP). No terreno há uma nascente, vegetação nativa – remanescente de Mata Atlântica – e fauna silvestre. 

                          <br><br>As APPs são protegidas pela Lei Florestal 12.651/2012 e servem para preservar a biodiversidade e os recursos naturais. O Bioma Mata Atlântica é considerado Patrimônio Nacional pela Constituição Federal e protegido pela Lei Federal 11.428/2006. São áreas que devem ser mantidas com sua cobertura vegetal nativa, sendo proibido remover, pois elas ajudam a manter a qualidade da água, proteger o solo da erosão, regular o clima e preservar a fauna e a flora para garantir a sustentabilidade ambiental. 
                          
                          <br><br>Em quase três décadas de tramitação, a proposta foi submetida a audiências públicas em 2002 e colocada em suspenso até 2006, quando a Belvedere Participações assinou um Termo de Ajustamento de Conduta com o Ministério Público. Na ocasião, os empreendedores se comprometeram a readequar o projeto por conta do impacto ambiental que causaria às reservas subterrâneas de água. Foi excluído, por exemplo, a implantação de um posto de combustíveis, pois no aquífero há uma fonte de água mineral, cuja exploração virou motivo de ação civil (ainda em tramitação) entre a empresa LVP Mineração Ltda – interessada em engarrafar a água – e a Condor Empreendimentos Imobiliários S.A, proprietária do terreno. 
                          
                          <br><br>Classificado como Projeto Especial de Impacto Urbano de 2º grau em função de sua complexidade, além de ter atividade elencada como potencialmente poluidora ao ambiente, o empreendimento gerou o primeiro Termo de Compromisso em 2016 e recebeu a Licença Prévia - documento que aprova a concepção e a localização do imóvel – para as duas torres comerciais e o shopping em 2018, depois que o ex-prefeito Nelson Marchezan Júnior anunciou, em 2017, o <a href="https://dopaonlineupload.procempa.com.br/dopaonlineupload/2080_ce_190631_1.pdf" target="_blank">decreto 19.741</a> que acelerou a análise de 89 projetos prioritários para a cidade, segundo ele. 
                          
                          <br><br>Em 2020, a extinta Secretaria Municipal do Meio Ambiente e da Sustentabilidade (Smams) também liberou a Licença de Instalação para o hipermercado do grupo Zaffari. Com área superior a 33 mil metros quadrados, <a href="https://prefeitura.poa.br/smams/noticias/prefeitura-libera-ultimas-licencas-de-instalacao-do-complexo-belvedere" target="_blank">segundo a prefeitura</a>, o empreendimento extrapola o tamanho determinado pela legislação do município, já que a Lei Complementar 462/2001 limitou o tamanho desse tipo de loja na capital a 2,5 mil m². 
                          
                          <br><br>As obras viárias acertadas em Termo de Compromisso, já estão em andamento, de acordo com a Procuradoria-Geral do Município. Segundo o órgão foi feita a implantação da Rua Diretriz 3133, com sentido único desde a Avenida Cristiano Fischer até a III Perimetral, qualificação de paradas de transporte coletivo “dentro da área de influência do empreendimento” e doação ao Município da área atingida pela alça da Rua José Carvalho Bernardes. Os documentos que comprovam a realização das obras, no entanto, não estão disponíveis para consulta pública.                          
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="destaque-6 col-4">
              <!-- Botão para abrir a modal 6 -->
              <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal6">
                Edifício Bewiki - 4° Distrito
              </button>

            <!-- Modal -->
              <div class="modal" data-bs-backdrop="false" id="modal6" tabindex="-1" aria-labelledby="modal6Label" aria-hidden="true">
                <div class="modal-dialog modal-lg"> <!-- Use a classe modal-lg para uma modal grande -->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="modal6Label">
                        Sem a liberação da aeronáutica, prefeitura de Porto Alegre quer construir o prédio mais alto da cidade na região do aeroporto</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        FECHAR
                      </button>
                    </div>
                    <div class="modal-body">
                      <span>
                        <em>Com previsão de 117 metros de altura, o empreendimento seria instalado a cerca de 5 km do Aeroporto Salgado Filho
                        </em>
                        <br><br>Um antigo moinho de farinha na esquina das ruas Sete de Abril e Emancipação, no bairro Floresta, dará lugar ao prédio mais alto de Porto Alegre nos próximos anos. O alvará de construção foi liberado em outubro do ano passado pela Secretaria Municipal do Meio Ambiente, Urbanismo e Sustentabilidade, mas a obra não foi autorizada pela Aeronáutica. A torre de 117 metros de altura não atende aos requisitos de segurança da Zona de Proteção de Aeródromo do Aeroporto Salgado Filho. Hoje, a altura máxima permitida para os edifícios no 4º Distrito, são 52 metros.

                        <br><br>O projeto arquitetônico desenvolvido pela empresa Bewiki Participações, foi o primeiro a ser beneficiado após a sanção da Lei Complementar 960/2022 que instituiu o Programa +4D de Regeneração Urbana do 4° Distrito que flexibiliza os parâmetros urbanísticos do Plano Diretor da cidade e concede benefícios para empreendimentos da região. A edificação prevê unidades residenciais, hospital, studios, coworking, supermercado, lojas e um rooftop, e deverá preservar a fachada do Moinho. Por sua relevância histórica, o edifício foi tombado pelo Instituto do Patrimônio Histórico e Cultural Estadual e não pode ser demolido.
                        
                        <br><br>O prédio, construído em 1942 para abrigar a fábrica de farinha Moinhos Germani, pertenceu à família de imigrantes italianos, Germani. Naquela época, o local era bastante requisitado para a instalação das indústrias, por conta de sua proximidade com os rios Gravataí e Delta do Jacuí que favoreciam o escoamento da produção. Além disso, ali passava uma antiga linha férrea que conectava a capital a outras regiões do Estado e facilitava o abastecimento dos grandes armazéns. Em 1990, o prédio foi vendido à empresa Molinos Incorporadora, que tem como sócia a BP Consultoria Empresarial, de Eduardo Gastaldo, proprietário da Bewiki, autora do projeto.  
                        
                        <br><br>Em 2015, ainda sob a gestão do ex-prefeito, José Fortunati, a Câmara Municipal de Porto Alegre aprovou o projeto de Lei Complementar que concedia isenção de impostos como IPTU, ITBI e ISSQN às empresas que se instalassem nos bairros Floresta, São Geraldo, Navegantes, Humaitá e Farrapos, com a intenção de revitalizar do 4º Distrito. Nesse mesmo ano, o executivo municipal firmou uma parceria com a Universidade Federal do Rio Grande do Sul para a criação de um projeto de requalificação do prédio-sede da antiga fábrica. Um ano depois, a UFRGS apresentou a proposta de criação do Complexo Moinhos Germani. Estavam previstos espaço gastronômico em pátio aberto, lounge, lavanderia, moradia compartilhada com lofts para aluguel e Coworking para atender estudantes, profissionais e empresas. O foco era desenvolvimento intersocial, crescimento econômico, valorização e preservação do patrimônio da cidade. O projeto não foi para frente. 
                         </span>
                    </div>
                  </div>
                </div>
              </div>
              </li>
              <li class="destaque-7 col-4">
                <!-- Botão para abrir a modal 7 -->
              <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal7">
               Fazenda do Arado
              </button>

            <!-- Modal -->
              <div class="modal" data-bs-backdrop="false" id="modal7" tabindex="-1" aria-labelledby="modal7Label" aria-hidden="true">
                <div class="modal-dialog modal-lg"> <!-- Use a classe modal-lg para uma modal grande -->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="modal7Label">
                        Com estudo de impacto ambiental considerado falso, omisso e enganoso, projeto para urbanizar a antiga Fazenda do Arado avança em Porto Alegre</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        FECHAR
                      </button>
                    </div>
                    <div class="modal-body">
                      <span>
                        <em>Um inquérito policial, de 2018, concluiu que houve omissão sobre a existência de Áreas de Preservação Permanente e fauna ameaçada de extinção, além de alteração nos mapas geológicos da fazenda</em>
                        <br><br>A reunião do Conselho do Plano Diretor (CMDUA) do dia <a href="https://www.youtube.com/watch?v=Z8kusK8r6Tw&list=PLSs49thZPnN6gS6JWc2CGye74byIulSzu&index=9&t=2820s" target="_blank">22 de agosto deste ano</a> – que aprovou o Estudo de Viabilidade Urbanística para urbanização da antiga Fazenda do Arado – estava no ar há 15 minutos quando o arquiteto José Rodolfo Fork, responsável técnico da proposta, usou seu tempo de fala para dizer que concordava que o projeto deveria ser discutido com honestidade intelectual, responsabilidade ética e social. “Quero dizer que concordo que essas premissas devem ser a base do debate. Por isso, é essencial ressaltar que há alegações equivocadas de que o projeto está expandindo da zona urbana para a zona rural, quando na verdade está situado em zona urbana rarefeita. A flora e fauna serão preservadas. Além disso, a alegação de que o projeto poderia causar inundações ao bairro Belém Novo também carece de fundamento”, disse. 

                        <br><br>Ao finalizar sua manifestação, Fork pediu que os demais conselheiros considerassem esses princípios ao tomarem suas decisões naquela noite. Além de representante do empreendedor, o arquiteto também tem um assento no Conselho do Plano Diretor. A empresa dele, a Fork Projetos e Construções, presta serviço à Arado Empreendimentos Imobiliários, proponente da proposta. 
                        
                        <br><br>O engenheiro Vinicius Galeazzi, conselheiro representante do Sindicato dos Engenheiros do Rio Grande do Sul, não esperou o início da votação – que ocorre no final da reunião – para emitir opinião contrária à implementação do projeto. Ao antecipar o voto, ele afirmou que a instituição [SENGE-RS] acredita que uma cidade ideal é compacta e equilibrada, e que deve ser economicamente viável e sustentável, o que não acontece quando há grandes distâncias e vazios urbanos. “A cidade merece ser inclusiva, democrática, não segregadora. Com esse projeto o município vai expandir, crescer, o que vai encarecer para todos os porto-alegrenses. Entendemos também que os Projetos Especiais devem ser avaliados de diversas perspectivas técnicas e sociais, deve ter vocação pública e ser exceção e não se proliferar pela cidade”, afirmou. 
                        
                        <br><br>Galeazzi também lembrou os colegas que a lei que aprovou o redesenho urbanístico do terreno não realizou estudo técnico para seu embasamento, há inquérito civil instaurado na 9ª Vara Federal de Porto Alegre, existe uma ação demarcatória da área indigena ainda em curso na justiça, além do estudo de impacto ambiental e seu relatório que não tiveram validade e por isso estão sendo investigados. “Não existe novo EIA/RIMA. É judicialmente inseguro levar adiante esse processo. Portanto, nesse momento, classificamos a continuidade da tramitação desse EVU como prematura e irresponsável”.
                        
                        <br><br>“O que embasa o projeto para que ele possa tramitar? Sugiro que o senhor Secretário tenha o cuidado de levar à Procuradoria-Geral do Município os questionamentos feitos pelo conselheiro Vinícius”, solicitou o conselheiro representante da Região de Planejamento 01, Felisberto Seabra. “O que há são tentativas vindas de opiniões divergentes e ingresso de ação judicial para tentar impedir a tramitação, mas nenhuma foi exitosa”, justificou o Secretário de Meio Ambiente, Urbanismo e  Sustentabilidade, Germano Bremm. “Além disso, a PGM faz parte da comissão que aprovou o EVU”, complementou o secretário. 
                        <br><br>Dez conselheiros ligados a entidades de classe, universidade e representantes da comunidade seguiram o mesmo entendimento e votaram contrários à aprovação do Estudo de Viabilidade Urbanística do empreendimento. Foram favoráveis dois membros da comunidade, um representante da área da construção civil e todos os oito representantes do governo, definindo pela aprovação do EVU. 
                        
                        <br><br>Em 2018, um inquérito policial foi aberto para apurar as denúncias de irregularidades e omissões no Estudo de Impacto Ambiental (EIA) e no Relatório de Impacto Ambiental (RIMA). A Polícia Civil concluiu ter havido omissões sobre a existência de fauna ameaçada de extinção, como o gato-maracajá. As irregularidades também envolviam os estudos sobre o estágio da Mata Atlântica na Ponta do Arado, além da baixa altura do terreno nas áreas próximas ao Guaíba, em nível inferior ao estipulado pelo Departamento de Esgotos Pluviais (DEP) para a construção de empreendimentos. A baixa altura exigiria um grande aterro dentro da Área de Preservação Permanente.
                        
                        <br><br>Esse mesmo estudo declarado em parte como “falso/enganoso/omisso” no laudo do Instituto Geral de Perícias (IGP), embasou o Projeto de Lei Complementar elaborado pelo governo de Sebastião Melo (MDB) para mudar o regime urbanístico da Fazenda do Arado, tornando-a adaptada para a construção do empreendimentos imobiliários, <a href="<?php echo get_site_url(); ?>/noticias/meio-ambiente/2021/10/projeto-na-zona-sul-de-porto-alegre-tem-estudo-de-impacto-ambiental-considerado-falso-e-omisso/" target="_blank">conforme apurou o Sul21</a>.
                        
                        <br><br>A previsão da empresa é separar os 426 mil metros quadrados do terreno em 2.300 lotes para a construção de imóveis, o que trará um aumento populacional de 70% para o bairro Belém Novo. A audiência pública, exigida pela legislação para alteração do regime urbanístico, foi feita de forma virtual em 2021 – 50 pessoas se inscreveram para falar, cerca de 20 eram moradores da região. A Prefeitura também ofereceu a sede do Centro dos Funcionários da Assembleia Legislativa para quem quisesse participar presencialmente, mas com espaço limitado a 60 pessoas por conta da pandemia de Covid-19.</span>
                    </div>
                  </div>
                </div>
              </div>
              </li>
              <li class="destaque-8 col-4">
                <!-- Botão para abrir a modal 8 -->
              <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal8">
                Golden Lake
               </button>
 
             <!-- Modal -->
               <div class="modal" data-bs-backdrop="false" id="modal8" tabindex="-1" aria-labelledby="modal8Label" aria-hidden="true">
                 <div class="modal-dialog modal-lg"> <!-- Use a classe modal-lg para uma modal grande -->
                   <div class="modal-content">
                     <div class="modal-header">
                       <h5 class="modal-title" id="modal8Label">
                        Golden Lake</h5>
                       <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                         FECHAR
                       </button>
                     </div>
                     <div class="modal-body">
                       <span>
                         <em>Exclusividade pode custar até 11 milhões</em>
                         <br><br>As obras do bairro privado de luxo Golden Lake, na beira do rio Guaíba, zona sul da capital, estão a pleno vapor. Ao todo, sete condomínios serão construídos pela Multiplan Empreendimentos Imobiliários na próxima década. O primeiro, Lake Victoria – já em construção –, terá quatro torres e 94 apartamentos. Ciclovias, pistas para caminhada, praia, lago, quadras esportivas, night club e restaurante garantem que os moradores não precisem conviver com a cidade lá fora. A empresa diz que o empreendimento coroa uma nova fase de Porto Alegre, “Nasceu para proporcionar um novo lifestyle aos gaúchos”.

                         <br><br>O Termo de Compromisso – contrato que estipula as obrigações do empreendedor para mitigar e compensar os impactos causados pela instalação do projeto – foi assinado em 2016, mas a infraestrutura de drenagem e pavimentação acordadas só iniciaram quatro anos mais tarde. O documento vinculou a entrega das obras ao registro de escritura de aquisição do terreno – “Considerando que o compromissário ainda não é o atual proprietário das áreas”. O tempo de espera parece ter valido a pena. Hoje, um único apartamento no condomínio pode <a href="https://www.foxterciaimobiliaria.com.br/imovel/210837" target="_blank">custar mais de 11 milhões</a>. 
                         
                         <br><br>O projeto, no entanto, não teria prosperado sem o apoio político que entregaria ao empresário José Isaac Peres o terreno para instalar os empreendimentos de alto padrão. A área pertencia ao governo do Estado, estava cedida ao Jockey Club do Rio Grande do Sul e não podia ser vendida. A ex-governadora Yeda Crusius (PSDB) resolveu o impasse <a href="https://estado.rs.gov.br/governadora-yeda-crusius-sanciona-projeto-de-lei-para-doacao-de-area-ao-jockey-club" target="_blank">doando as terras ao clube</a>, em 2010, com apoio unânime da Assembleia Legislativa gaúcha. 
                         
                         <br><br>Um ano depois, ainda faltava ao Jockey uma fração de terra que pertencia ao município para juntar aos 166 mil metros quadrados do Clube e viabilizar o negócio. O Executivo Municipal tinha cerca de 12 mil m², “remanescente da implantação da Avenida Diário de Notícias” que interessava à Multiplan. Uma permuta proposta pelo ex-prefeito José Fortunati (PDT) entregou o terreno em troca um imóvel de 22 mil m² junto às avenidas Icaraí e Chuí. A prefeitura ainda precisou <a href="https://www2.portoalegre.rs.gov.br/cgi-bin/nph-brs?s1=000032547.DOCN.&l=20&u=%2Fnetahtml%2Fsirel%2Fsimples.html&p=1&r=1&f=G&d=atos&SECT1=TEXT" target="_blank">pagar R$ 24,5 mil</a> referente a diferença de valor.
                         
                         <br><br>O próximo passo foi conseguir apoio parlamentar para não permitir a construção de habitações de interesse social na área. À época, a cidade se preparava para receber a Copa do Mundo de Futebol de 2014 e uma das obras previstas para garantir maior fluidez no trânsito entre a zona Sul e restante da cidade, era a duplicação da avenida Tronco. Para executar as obras, a Prefeitura removeu mais de 1500 famílias. 
                         
                         <br><br>Em 2011, ainda sem um lugar definitivo para morar, a comunidade fez apelo aos representantes da Câmara Municipal para que se comprometessem em transformar parte da área do Jockey em Área Especial de Interesse Social (AEIS) e desta forma assentar as famílias. Não foram atendidas. “Eu ouvi do Fogaça, ouvi do Fortunati e de toda a sua equipe que a obra da Tronco não sairá, se, primeiro, não estiver resolvida a vida das pessoas. Isso é garantia; isso é compromisso!”, justificou o ex-vereador Sebastião Melo na sessão de votação que aprovou a permuta do terreno público. 
                         
                         <br><br>Doze anos depois, como uma sina, a situação das famílias <a href="https://diariogaucho.clicrbs.com.br/dia-a-dia/noticia/2023/03/projeto-de-loteamentos-para-familias-da-tronco-depende-de-novas-portarias-do-minha-casa-minha-vida-24833795.html" target="_blank">permanece incerta</a> e as obras da Tronco ainda não foram finalizadas, mas o dono da Multiplan recebeu os 178 mil metros quadrados para povoar o bairro de luxo. A generosidade do Estado renderá <a href="https://vipfiles.valor.com.br/BDEmpresas/cc8fe3c8-84d4-431e-8298-65388f24b60a.pdf" target="_blank">cerca de R$ 164 milhões</a> ao Jockey e outros <a href="https://guaiba.com.br/2021/10/06/porto-alegre-com-aporte-de-r-5-bi-bairro-privativo-na-zona-sul-preve-r-250-milhoes-em-acoes-de-contrapartida/" target="_blank">R$ 4 bilhões</a> ao dono da Multiplan. 
                         
                         <br><br>Para mitigar e compensar os impactos causados pelo novo bairro na zona sul, a empresa ficou responsável por uma lista de 35 obras de circulação e acessibilidade, duas de drenagem, sinalização viária, melhoramento na praça José Alexandre Záchia, recursos para a Unidade de Conservação Parque Natural do Morro do Osso, reformar seis escolas municipais, adquirir equipamentos para a Secretaria Municipal de Saúde, fazer melhorias na unidade de saúde Família Nossa Senhora das Graças, comprar um imóvel para uma Unidade Destino Certo, equipamentos para o sistema de segurança pública e para Biblioteca Comunitária do Cristal. Além de desenvolver projeto de monitoramento e salvamento arqueológicos da obra, pesquisa e prospecção arqueológica. 
                         
                         <br><br>Outras contrapartidas foram acordadas em Termo de Conversão em Área Pública – a legislação determina que parte da área de grandes empreendimentos deve ser destinada ao Poder Público para a implantação de equipamentos públicos, como escolas ou praças. As obras no Centro de Qualificação da Cruzeiro, uma nova subestação de energia elétrica e calçamento para o Pronto Atendimento Cruzeiro do Sul, restauração da Casa Godoy e outros projetos para a orla do Guaíba estão nesse documento. Essas obras ainda não foram realizadas porque o contrato feito pela Prefeitura, estipulou que elas seriam realizadas após 36 meses do início das obras de cada uma das torres do empreendimento, o que acontecerá em diferentes fases. 
                         
                         <br><br>Todas as obras e serviços, foram avaliadas em cerca de R$ 170 milhões. Caso as projeções de lucro da empresa se concretizem, os valores gastos em contrapartidas corresponderiam a cerca de 4,25% do valor geral de vendas do empreendimento. 
                         
                         <br><br>Embora o bairro privativo de luxo Golden Lake seja algo inédito na história da cidade, a bilionária Multiplan é uma velha conhecida. Peres inaugurou seus negócios em Porto Alegre em 2008, com a construção do Barra Shopping Sul, também no bairro Cristal. Em 2013, quando planejava a expansão do shopping, o empresário foi alvo da Operação Concutare da Polícia Federal. Em 10 meses de investigação, a PF identificou diversas quadrilhas que atuavam no Rio Grande do Sul. A rede de corrupção envolvia empresários e consultores ambientais que aliciavam funcionários públicos para acelerar as licenças ambientais ou desconsiderar os estudos de impacto ambiental dos projetos. 
                         
                         <br><br>Na época, o futuro Golden Lake também entrou na mira da Polícia Federal, pois havia suspeita de que as compensações acordadas para a construção das torres não estavam de acordo com a legislação vigente. 
                         
                         <br><br>Entre os 49 indiciados, estava toda a cúpula ambiental do do governo do Estado e da prefeitura. Carlos Fernando Niedersberg (PC do B), ex-secretário estadual do Meio Ambiente, Berfran Rosado (PPS), consultor ambiental e ex-secretário estadual de Meio Ambiente e Luiz Fernando Záchia (PMDB). Segundo o inquérito, o empresário poderia ter sido beneficiado com a aceleração da licença para a obra do shopping com apoio de Záchia. Ele foi um dos políticos presos, <a href="https://portoimagem.wordpress.com/2013/05/03/shopping-de-porto-alegre-nega-pedido-de-licenciamento-ambiental/" target="_blank">sob suspeita de receber R$ 20 mil</a>, mas a demora do Ministério Público Federal em apresentar a denúncia de parte dos investigados, seis anos depois de concluída a investigação, fez a Justiça Federal arquivar o inquérito. </span>
                     </div>
                   </div>
                 </div>
               </div>
              </li>
              <li class="destaque-9 col-4">
                <!-- Botão para abrir a modal 9 -->
              <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal9">
                Iguatemi
               </button>
 
             <!-- Modal -->
               <div class="modal" data-bs-backdrop="false" id="modal9" tabindex="-1" aria-labelledby="modal9Label" aria-hidden="true">
                 <div class="modal-dialog modal-lg"> <!-- Use a classe modal-lg para uma modal grande -->
                   <div class="modal-content">
                     <div class="modal-header">
                       <h5 class="modal-title" id="modal9Label">
                        Falta de planejamento da Prefeitura permitiu ao Shopping Iguatemi inaugurar áreas ampliadas sem entregar obras viárias à cidade</h5>
                       <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                         FECHAR
                       </button>
                     </div>
                     <div class="modal-body">
                       <span>
                         <em>O município alegou problemas financeiros e não cumpriu com a desapropriação do terreno necessário para o prolongamento da rua Anita Garibaldi, atrasando a obra em dez anos</em>
                         <br><br>A obra de prolongamento da rua Anita Garibaldi, inaugurada em setembro de 2022, foi uma das obrigações assumidas pela Administradora Gaúcha de Shopping Center quando solicitou ao município a ampliação do Iguatemi ainda em 2013. O shopping construiu 100 novas lojas, uma torre comercial de 14 andares e 1,3 mil vagas de estacionamento. Para absorver o aumento do tráfego, a empresa se comprometeu com uma série de obras viárias que não puderam ser executadas por conta da falta de planejamento do Município. 

                         <br><br>Ao assinar, em 2013, o Termo de Compromisso que acordava as obrigações decorrentes do licenciamento da obra, a Prefeitura ficou responsável por fazer a desapropriação dos terrenos necessários para a criação das novas ruas, sem fazer o devido planejamento. O custo de R$ 32 milhões – segundo a Secretaria Municipal da Fazenda a época – acabou sendo um impeditivo para que o executivo municipal conseguisse cumprir com o pactuado.
                         
                         <br><br>Três anos depois, em 2016, e novamente em 2018, um novo acordo precisou ser firmado para rever as obras de circulação e acessibilidade e propor a flexibilização do cronograma, “face a não execução das desapropriações”. O documento ainda considerava que todas as demais exigências assumidas e que não demandavam a liberação dos terrenos haviam sido executadas pelo empreendedor. O Município acabou entregando o habite-se para o Shopping mesmo sem a realização das medidas necessárias para mitigar o impacto do empreendimento e amenizar os problemas de congestionamento de tráfego da região.
                         
                         <br><br>Dez anos em negociações foram necessários para que a empresa executasse parte das obras previstas no contrato. Segundo a Procuradoria-Geral do Município, o primeiro trecho de 900 metros, entre a rua Carlos Legori e a avenida João Wallig, foi entregue no ano passado. Já para a duplicação do segmento final, ainda é necessária a desapropriação de 14 imóveis particulares. Atualmente, dez desses processos são tratados administrativamente pela Prefeitura, e os demais correm por via judicial. Outras ações previstas, como a liberação completa da área doada junto ao Country Club e a conexão com a Avenida Túlio de Rose, através da Avenida João Wallig, a partir da Rua General Pedro Bittencourt, também foram concluídas. </span>
                     </div>
                   </div>
                 </div>
               </div>
              </li>
              <li class="destaque-10 col-4">
                <!-- Botão para abrir a modal 10 -->
              <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal10">
                Pontal
               </button>
 
             <!-- Modal -->
               <div class="modal" data-bs-backdrop="false" id="modal10" tabindex="-1" aria-labelledby="modal10Label" aria-hidden="true">
                 <div class="modal-dialog modal-lg"> <!-- Use a classe modal-lg para uma modal grande -->
                   <div class="modal-content">
                     <div class="modal-header">
                       <h5 class="modal-title" id="modal10Label">
                        Entre vetos da população e denúncias de lobby empresarial, complexo Pontal saiu do papel quase 20 anos depois da venda do terreno</h5>
                       <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                         FECHAR
                       </button>
                     </div>
                     <div class="modal-body">
                       <span>
                         <em>A área onde está instalado o empreendimento passou por disputas e mudança na legislação urbanística para ser vendida à iniciativa privada em 2005</em>
                         <br><br>O Complexo Pontal, inaugurado em abril deste ano, foi projetado pela incorporadora Melnick em parceria com a BM Par Empreendimentos Imobiliários. Dividido em shopping, hotel, escritórios, consultórios médicos, centro de eventos, estacionamento, loja de material de construção e um parque público – este último era uma obrigação urbanística a ser entregue pelo empreendedor. A execução do projeto, contudo, só foi possível após modificação na legislação que alterou o padrão de construção permitido no local para tornar o negócio mais atrativo aos compradores. 

                         <br><br>A área pública, de 42 mil metros quadrados, que até 1995 estava cedida à fábrica de navios Estaleiro Só, passou por disputas ao longo de vários anos. Após a falência da empresa, o terreno voltou para o poder público municipal. Na década seguinte, a prefeitura tentou leiloá-lo algumas vezes, mas só conseguiu após o então prefeito Tarso Genro (PT) sancionar a <a href="https://leismunicipais.com.br/a/rs/p/porto-alegre/lei-complementar/2002/47/470/lei-complementar-n-470-2002-cria-a-subunidade-de-estruturacao-urbana-03-da-ueu-4036-referente-a-area-do-estaleiro-so-define-seu-regime-urbanistico-e-da-outras-providencias" target="_blank">Lei Complementar 470/2002</a>, que retirou a restrição de atividade exclusivamente naval do local. 
                         
                         <br><br>Avaliado em R$ 17 milhões [em valores da época], acabou arrematado pelo lance mínimo de R$ 7,2 milhões pelo empresário Saul Boff, dono da SVB Participações – única empresa a se interessar pelo negócio. O empreendedor então apresentou à prefeitura o projeto Pontal do Estaleiro, que previa, além de imóveis comerciais, edifícios residenciais, o que contrariava a lei complementar de 2002. “A ideia era fazer um empreendimento misto, para isso eu teria de mudar a lei porque, na época, só podia construir área comercial”, declarou Boff <a href="https://gauchazh.clicrbs.com.br/colunistas/marta-sfredo/noticia/2019/03/a-origem-da-fortuna-e-os-negocios-peculiares-do-empresario-por-tras-do-projeto-do-pontal-cjsuoquzi006l01qkziswcaph.html" target="_blank">em entrevista à GZH</a>. 
                         
                         <br><br>Os interesses do empresário foram, novamente, levados a plenário e, em novembro de 2008,  a Câmara Municipal aprovou mais uma vez a alteração da legislação autorizando o uso residencial, mas uma denúncia de pagamento de propina acabou com os planos de Boff. “Recebi oferta de ajuda para minha campanha eleitoral por um emissário da BM Par”, revelou o ex-vereador Cláudio Sebenelo (PSDB) em entrevista ao jornalista Juremir Machado da Silva para o Jornal Correio do Povo. Segundo ele, um envelope de dinheiro chegou a ser colocado em sua mesa. 
                         
                         <br><br>O advogado da empresa, Milton Terra Machado, negou ter havido suborno. “A oferta de doação não se vinculou a um pedido de aprovação do projeto”, <a href="https://www1.folha.uol.com.br/fsp/cotidian/ff2411200821.htm" target="_blank">disse à Folha de São Paulo</a>. Segundo ele, o vereador tucano era amigo de um diretor da BM Par. O Ministério Público gaúcho alegou falta de provas e arquivou o processo. 
                         
                         <br><br>Para encerrar a polêmica, o ex-prefeito José Fogaça (PMDB) decidiu realizar um plebiscito e chamar a população para definir o destino da área. Em agosto de 2009, 22.619 mil eleitores compareceram às urnas e 18.212 mil decidiram que o local deveria continuar público e que não poderia receber prédios residenciais, obrigando a empresa a mudar o projeto. As obras, entretanto, só começaram em 2019, com o documento de Licença de Instalação entregue em cerimônia promovida pelo ex-prefeito Nelson Marchezan Junior (PSDB). De acordo com o empresário, a demora se deu por cautela. “Claro, todo mundo com medo que desse algum problema”. 
                         
                         <br><br>Na assinatura do Termo de Compromisso – contrato que estabelece as obrigações do empreendedor com o município –, a empresa assumiu um conjunto de 24 medidas para atenuar os impactos negativos à região decorrentes da instalação do empreendimento de grande porte, 14 delas foram obras de infraestrutura viária, como ruas de acesso ao empreendimento e passagem para pedestre. As demais, resumiram-se à implantação de duas paradas de transporte coletivo, uma próxima ao Museu Iberê Camargo, que em 2022 foi substituída por “doar e implantar paradas na Avenida Tronco”, e outra junto ao Parque do Pontal. A empresa também deveria executar readequações na Estação de Bombeamento de Esgoto para suportar o novo fluxo local e obras de proteção contra cheias. 
                         
                         <br><br>A Procuradoria-Geral do Município – responsável por monitorar a entrega das obrigações – informou que somente o plano cicloviário permanece em aberto, mas não forneceu as documentações que comprovam a execução das obras. 
                         
                         <br><br>A propriedade, mesmo privada, está localizada na orla do Guaíba, uma das regiões mais nobres da cidade por ter grande interesse ambiental ao município. As margens do rio estão sob a proteção da Lei Orgânica do Município – <a href="https://leismunicipais.com.br/lei-organica-porto-alegre-rs" target="_blank">Art. 245, inciso V</a> – e do Código Florestal Ambiental – <a href="https://www.planalto.gov.br/ccivil_03/_ato2011-2014/2012/lei/l12651.htm" target="_blank"></a> 4º, inciso I, que consideram as orlas, Áreas de Preservação Permanente. A última revisão do Plano Diretor de Desenvolvimento Urbano e Ambiental de Porto Alegre (Lei Complementar 434/1999), no <a href="https://lproweb.procempa.com.br/pmpa/prefpoa/spm/usu_doc/planodiretortexto.pdf" target="_blank">Art. 154, inciso XVI</a>, estabeleceu o prazo de 12 meses para o município realizar o zoneamento ambiental da orla do Guaíba, dando indícios de que a região deveria ser protegida, o que nunca foi feito. 
                         
                         <br><br>As belezas naturais do Guaíba alavancaram os negócios das empresas Melnick e BM Par que puderam comercializar o “primeiro complexo multiuso privado banhado pelo Guaíba”. Todo o complexo hoteleiro foi disponibilizado para investidores, que poderiam comprar suítes inteiras ou “partes ideais” – quando um imóvel tem vários donos. Em junho de 2020, conforme tabela de valores da incorporadora Melnick, uma suíte de 31 metros quadrados podia ser adquirida por cerca de R$ 968 mil. 
                         
                         <br><br>As salas de escritórios e consultórios médicos da Torre Pontal foram postas à venda ainda na fase de construção do empreendimento por R$ 742 mil. De acordo com a última apresentação de resultados da empresa, 90% das 237 unidades comerciais foram vendidas, além de 94% do hotel. As 163 lojas do shopping foram disponibilizadas para aluguel. </span>
                     </div>
                   </div>
                 </div>
               </div>
              </li>
              <li class="destaque-11 col-4">
                <!-- Botão para abrir a modal 11 -->
              <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal11">
                Square Garden
               </button>
 
             <!-- Modal -->
               <div class="modal" data-bs-backdrop="false" id="modal11" tabindex="-1" aria-labelledby="modal11Label" aria-hidden="true">
                 <div class="modal-dialog modal-lg"> <!-- Use a classe modal-lg para uma modal grande -->
                   <div class="modal-content">
                     <div class="modal-header">
                       <h5 class="modal-title" id="modal11Label">
                        Antigo Ginásio da Brigada Militar de Porto Alegre será transformado em complexo residencial e comercial de R$ 250 milhões</h5>
                       <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                         FECHAR
                       </button>
                     </div>
                     <div class="modal-body">
                       <span>
                         <em>Os planos da empresa, no entanto, dependem de mudanças no Plano Diretor de Porto Alegre para se concretizar</em>
                         <br><br>Na esquina da avenida Ipiranga com a Silva Só, no bairro Santa Cecília, região central da cidade, o parque gastronômico Square Garden, parceria entre a incorporadora Melnick e o Grupo Amiche, foi programado para funcionar durante um ano enquanto o grupo aguarda “definições do Plano Diretor para a região”, segundo um dos corretores de imóveis da Melnick. O diretor de incorporação da empresa, Marcelo Guedes, <a href="https://gauchazh.clicrbs.com.br/colunistas/marta-sfredo/noticia/2020/12/como-e-o-projeto-de-r-250-milhoes-da-empresa-que-assumiu-o-terreno-do-ginasio-da-brigada-ckialfldb0059017wqvb9o1xr.html" target="_blank">disse à GZH</a> que o projeto final ainda não está definido, mas a ideia é instalar ali uma das torres mais altas da cidade. “Há perspectiva que possam ser construídas três torres de 17 andares”. A legislação atual permite construções de no máximo 52 metros de altura no local. 

                         <br><br>Na área de quase 10 mil metros quadrados onde está localizado o food park itinerante, havia o antigo Ginásio da Brigada Militar de Porto Alegre. O imóvel teve parte do telhado destruído em um temporal em 2017 e o governo do Estado decidiu não reformar o prédio, colocando o terreno em leilão. Como não houve interessados, o ex-governador José Ivo Sartori (PMDB) decidiu entregar o imóvel em permuta à empresa Verdi Sistemas Construtivos. Em troca, recebeu uma penitenciária em Sapucaia do Sul, na Região Metropolitana, que custou à Verdi pouco mais de R$ 44 milhões. 
                         
                         <br><br>A venda gerou protestos por parte de ex-comandantes-gerais e ex-oficiais da Brigada Militar que cobraram a preservação do patrimônio público. Parte da corporação tentou, sem sucesso, impedir a derrubada do prédio construído em 1963.  O local funcionou por quase 55 anos vinculado à Escola de Educação Física da Brigada Militar, que sediava eventos dos Jogos Mundiais Universitários e era utilizado pela comunidade. 
                         
                         <br><br>Em 2020, a Verdi fez também uma permuta com a Melnick para viabilizar a construção do complexo com imóveis residenciais, hotel e shopping, com valor geral de vendas (potencial de lucro que o projeto pode render) estimado em R$ 250 milhões.</span>
                     </div>
                   </div>
                 </div>
               </div>
              </li>
              <li class="destaque-12 col-4">
                <!-- Botão para abrir a modal 11 -->
              <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal12">
                Torres do Beira-Rio
               </button>
 
             <!-- Modal -->
               <div class="modal" data-bs-backdrop="false" id="modal12" tabindex="-1" aria-labelledby="modal12Label" aria-hidden="true">
                 <div class="modal-dialog modal-lg"> <!-- Use a classe modal-lg para uma modal grande -->
                   <div class="modal-content">
                     <div class="modal-header">
                       <h5 class="modal-title" id="modal12Label">
                        Com projeto de lei parado na Câmara de Vereadores torres ao lado do estádio Beira-Rio não tem data para acontecer</h5>
                       <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                         FECHAR
                       </button>
                     </div>
                     <div class="modal-body">
                       <span>
                         <em>Construção do empreendimento depende de mudança na legislação </em>
                         <br><br>Em dezembro de 2017, o Sport Club Internacional encomendou formalmente à Prefeitura de Porto Alegre, por meio de seu presidente à época Marcelo Medeiros, um projeto de lei que  autorizasse “a realização de um empreendimento imobiliário a ser comercializado” pelo clube esportivo. O imóvel ocuparia 25 mil metros quadrados da quadra onde está o Estádio e que hoje serve de estacionamento entre as ruas Fernando Lúcio Costa, avenida Padre Cacique, rua Carlos Medina e avenida Edvaldo Pereira Paiva. Entretanto, o terreno público foi doado ao Internacional em 1956 pelo então prefeito Leonel Brizola para a construção do futuro Complexo Beira-Rio na condição de ser usado somente com fins esportivos. 

                         <br><br>O projeto enviado pelo clube à prefeitura prevê dois edifícios para uso comercial e residencial e estacionamento para mais de 400 veículos. A maior torre pode chegar a 130 metros de altura, o que contraria as regras previstas no Plano Diretor da cidade, que determina altura máxima de 52 metros na orla do Guaíba. Além disso, a construção de prédios residenciais na orla não é permitida, inclusive rejeitada em consulta popular para outras edificações, como o Complexo Pontal.
                         
                         <br><br>Para atender a demanda do Inter, em 2018, o então prefeito Nelson Marchezan Júnior (PSDB) enviou projeto de lei para Câmara de Vereadores para <a href="https://www.camarapoa.rs.gov.br/noticias/projeto-autoriza-empreendimento-em-area-do-complexo-beira-rio" target="_blank">autorizar a realização do empreendimento imobiliário</a>. O PL incluía um novo artigo na lei municipal 1.651/56 permitindo a entidade privada construir na área que lhe foi doada com restrições. Marchezan também pretendia retirar da lei 511/2004 a denominação de Áreas Especiais de Interesse Institucional do local que hoje deve ser destinado a equipamentos de atendimento à população como universidades, hospitais e clubes, por exemplo. 
                         
                         <br><br>Enviado na gestão passada, mas sem avanços, o documento foi recuperado pelo prefeito Sebastião Melo (MDB), que deu novo texto ao Projeto de Lei 04/2019 para tentar mudar a legislação. Parado na Câmara de Vereadores desde 2021, o PL ainda não prosperou.</span>
                     </div>
                   </div>
                 </div>
               </div>
              </li>
            </ul>
          </div>

          <!-- Construtoras -->
          <div class="menu-builders" style="display: none">
            <div class="submenu-construtoras row justify-content-center">
              <div
                class="seta-voltar col-1"
                onclick="toggleVisibilityBuilders()"
              >
                <img src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/seta-voltar.svg" alt="" />
              </div>
              <p class="col-10">Construtoras</p>
              <div class="submenu-icone col-1">
                <img
                  src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/construtoras-icone.svg"
                  alt="Botão Construturas"
                />
              </div>
            </div>

            <ul class="construtoras row">
              <li class="construtora-1 col-4">
                <button type="button" onclick="melnickOnClick()">
                  Melnick
                </button>
              </li>
              <li class="construtora-2 col-4">
                <button type="button" onclick="cyrellaOnClick()">
                  Cyrela
                </button>
              </li>
              <li class="construtora-3 col-4">
                <button type="button" onclick="zaffariOnClick()">
                  Zaffari
                </button>
              </li>
              <li class="construtora-4 col-4">
                <button type="button" onclick="tendaOnClick()">Tenda</button>
              </li>
              <li class="construtora-5 col-4">
                <button type="button" onclick="lyxOnClick()">Lyx</button>
              </li>
              <li class="construtora-6 col-4">
                <button type="button" onclick="encorpOnClick()">Encorp</button>
              </li>
              <li class="construtora-7 col-4">
                <button type="button" onclick="mrvOnClick()">MRV</button>
              </li>
              <li class="construtora-8 col-4">
                <button type="button" onclick="maiojamaOnClick()">
                  Maiojama
                </button>
              </li>
              <li class="construtora-9 col-4">
                <button type="button" onclick="othersOnClick()">Outras</button>
              </li>
            </ul>
            <div class="divExpandButton">
              <button
                class="expandButton"
                id="expandButtonBuilders"
                type="button"
                onclick="expandMenuBuilders()"
              >
                Expandir <i class="fas fa-arrow-down"></i>
              </button>
            </div>
          </div>

          <!-- Estagios -->
          <div class="menu-stage" style="display: none">
            <div class="submenu-estagios row justify-content-center">
              <div class="seta-voltar col-1" onclick="toggleVisibilityStage()">
                <img src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/seta-voltar.svg" alt="" />
              </div>
              <p class="col-10">Estágio da Obra</p>
              <div class="submenu-icone col-1">
                <img
                  src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/obra-icone.svg"
                  alt="Botão Estágio da Obra"
                />
              </div>
            </div>

            <ul class="estagios row">
              <li class="estagio-1 col-12">
                <button type="button" onclick="concludedOnClick()">
                  Concluída
                </button>
              </li>
              <li class="estagio-2 col-12">
                <button type="button" onclick="designedOnClick()">
                  Projetada
                </button>
              </li>
              <li class="estagio-3 col-12">
                <button type="button" onclick="inProgressOnClick()">
                  Em Andamento
                </button>
              </li>
            </ul>
            <div class="divExpandButton">
              <button
                class="expandButton"
                id="expandButtonStage"
                type="button"
                onclick="expandMenuStage()"
              >
                Expandir <i class="fas fa-arrow-down"></i>
              </button>
            </div>
          </div>

          <!-- Macrozonas -->
          <div class="menu-macrozones" style="display: none">
            <div class="submenu-macrozonas row justify-content-center">
              <div
                class="seta-voltar col-1"
                onclick="toggleVisibilityMacrozones()"
              >
                <img src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/seta-voltar.svg" alt="" />
              </div>
              <p class="col-10">Macrozonas</p>
              <div class="submenu-icone col-1">
                <img
                  src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/macrozonas-icone.svg"
                  alt="Botão Macrozonas"
                />
              </div>
            </div>

            <ul class="macrozonas row">
              <li class="macrozona-1 col-6">
                <button type="button" onclick="MZ1OnClick()">MZ 01</button>
              </li>
              <li class="macrozona-2 col-6">
                <button type="button" onclick="MZ5OnClick()">MZ 05</button>
              </li>
              <li class="macrozona-3 col-6">
                <button type="button" onclick="MZ2OnClick()">MZ 02</button>
              </li>
              <li class="macrozona-4 col-6">
                <button type="button" onclick="MZ7OnClick()">MZ 07</button>
              </li>
              <li class="macrozona-5 col-6">
                <button type="button" onclick="MZ3OnClick()">MZ 03</button>
              </li>
              <li class="macrozona-6 col-6">
                <button type="button" onclick="MZ8OnClick()">MZ 08</button>
              </li>
              <li class="macrozona-7 col-6">
                <button type="button" onclick="MZ4OnClick()">MZ 04</button>
              </li>
              <li class="macrozona-8 col-6">
                <button type="button" onclick="MZ10OnClick()">MZ 10</button>
              </li>
            </ul>
            <div class="divExpandButton">
              <button
                class="expandButton"
                id="expandButtonMZ"
                type="button"
                onclick="expandMenuMacrozones()"
              >
                Expandir <i class="fas fa-arrow-down"></i>
              </button>
            </div>
          </div>
        </div>

        <!-- Cabeçalhos das legendas -->
        <div class="submenu">
          <!-- Cabeçalho legenda Projetos em Destaque -->
          <div class="menu-highlights" style="display: none">
            <div class="submenu-destaques row justify-content-center">
              <div
                class="seta-voltar col-1"
                onclick="toggleVisibilityHighlights()"
              >
                <img src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/seta-voltar.svg" alt="" />
              </div>
              <p class="col-10">Projetos em Destaque</p>
              <div class="submenu-icone col-1">
                <img
                  src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/destaque-icone.svg"
                  alt="Botão Projetos em Destaque"
                />
              </div>
            </div>
          </div>

          <!-- Cabeçalho legenda Construtoras -->
          <div class="menu-builders" style="display: none">
            <div class="submenu-construtoras row justify-content-start">
              <div
                class="seta-voltar col-1"
                onclick="toggleVisibilityBuilders()"
              >
                <img src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/seta-voltar.svg" alt="" />
              </div>
              <p class="col-10">Melnick</p>
              <div class="submenu-icone col-1">
                <img
                  src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/construtoras-icone.svg"
                  alt="Botão Construturas"
                />
              </div>
            </div>
          </div>

          <!-- Cabeçalho legenda Estágios da Obra -->
          <div class="menu-stage" style="display: none">
            <div class="submenu-estagios row justify-content-start">
              <div class="seta-voltar col-1" onclick="toggleVisibilityStage()">
                <img src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/seta-voltar.svg" alt="" />
              </div>
              <p class="col-10">Obras Concluídas</p>
              <div class="submenu-icone col-1">
                <img
                  src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/obra-icone.svg"
                  alt="Botão Estágio da Obra"
                />
              </div>
            </div>
          </div>

          <!-- Cabeçalho legenda Macrozonas -->
          <div class="menu-macrozones" style="display: none">
            <div class="submenu-macrozonas row justify-content-start">
              <div
                class="seta-voltar col-1"
                onclick="toggleVisibilityMacrozones()"
              >
                <img src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/seta-voltar.svg" alt="" />
              </div>
              <p class="col-10">Macrozona 01</p>
              <div class="submenu-icone col-1">
                <img
                  src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/icones/macrozonas-icone.svg"
                  alt="Botão Macrozonas"
                />
              </div>
            </div>
          </div>
        </div>
      </nav>
    </header>

    <main>
      <div class="text-center text-white">
        <!-- Home -->
        <div id="home" class="min-vw-100 min-vh-100 d-flex align-items-center">
          <div class="container">
            
            <h1 class="logos21 col-12">
              <img
                class="img-fluid"
                width="300"
                height="287"
                src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/logoDonos.png"
                alt="Donos da Cidade"
              />
            </h1>

            <div class=" col-12">
              <h2>Donos da Cidade</h2>
              <p>
                A quem serve o poder público de uma cidade? Mapeamos os projetos imobiliários especiais lançados em Porto Alegre na última década e mostramos como leis foram criadas e regras urbanísticas alteradas para atender interesses de alguns poucos empresários.
              </p>
            </div>

            <img class="dedo" src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/img/dedo-scroll-40x26.svg" alt="" />
            
          </div>
        </div>

        <!-- Botão voltar ao topo -->
        <button
          onclick="goTop()"
          id="goTopBtn"
          style="
            position: fixed;
            bottom: 20px;
            right: 45px;
            z-index: 2000;
            border: none;
            outline: none;
            background-color: #697476;
            color: white;
            cursor: pointer;
            padding: 12px 20px;
            border-radius: 50px;
          "
          title="Go to top"
        >
          <i class="fas fa-arrow-up"></i>
        </button>
      </div>

      <!-- Mapa -->
      <div id="map">
        <div id="mapa"></div>
        <!-- Legendas -->
        <div class="container d-flex align-items-center justify-content-center">
          <!-- Legenda Destaque -->
          <section class="legendas" style="display: block">
            <!-- Lista de Destaques -->
            <div class="lista col-12" id="listaLegendas">
              <ul class="row .no-gutters">
                <li class="legenda-cyrela col-4">
                  <p>Cyrela</p>
                </li>
                <li class="legenda-melnick col-4">
                  <p>Melnick</p>
                </li>
                <li class="legenda-zaffari col-4">
                  <p>Zaffari</p>
                </li>
                <li class="legenda-tenda col-4">
                  <p>Tenda</p>
                </li>
                <li class="legenda-lyx col-4">
                  <p>Lyx</p>
                </li>
                <li class="legenda-mrv col-4">
                  <p>MRV</p>
                </li>
                <li class="legenda-encorp col-4">
                  <p>Encorp</p>
                </li>
                <li class="legenda-maiojama col-4">
                  <p>Maiojama</p>
                </li>
                <li class="legenda-outras col-4">
                  <p>Outras</p>
                </li>
              </ul>
            </div>

            <div class="construtoraLegenda col-12" id="cyrellaLegenda">
              <h3>CYRELA</h3>
              <p>
                A construtora tem até o momento 30 empreendimentos médio alto
                padrão aprovados na capital, 19 deles estão na Macrozona 1, na
                região entre o Centro Histórico e a Terceira Perimetral. 10
                estão na Macrozona 3, que tem como limites a avenida Sertório e
                a cidade de Alvorada, e um está na Macrozona 2, limitada pelas
                avenidas Assis Brasil e Sertório.
              </p>
            </div>

            <div class="construtoraLegenda col-12" id="melnickLegenda">
              <h3>MELNICK</h3>
              <p>
                A incorporadora tem até o momento 32 empreendimentos de médio
                alto padrão aprovados na capital, 16 deles estão na Macrozona 1,
                na região entre o Centro Histórico e a Terceira Perimetral, 10
                estão na Macrozona 3, que tem como limites a avenida Sertório e
                a cidade de Alvorada. Os demais estão espalhados nas Macrozonas
                2, 4 e 5.
              </p>
            </div>
            <div class="construtoraLegenda col-12" id="zaffariLegenda">
              <h3>ZÁFFARI</h3>
              <p>
                A empresa possui até o momento, 12 empreendimentos aprovados na
                capital, nove deles estão na Macrozona 1, na região entre o
                Centro Histórico e a Terceira Perimetral. Outros quatro projetos
                estão divididos entre as Macrozonas 2, 3 e 5, nos bairros Passo
                d'Areia, Cristo Redentor, São João e Cavalhada.
              </p>
            </div>
            <div class="construtoraLegenda col-12" id="encorpLegenda">
              <h3>ENCORP</h3>
              <p>
                A construtora tem até o momento seis empreendimentos aprovados
                na capital, dois deles para Demanda Habitacional Prioritária.
                Localizados na Macrozona 4, que compreende desde o bairro
                Cristal até os bairros Agronomia e Morro Santana. Outros dois
                estão na Macrozona 5, no bairro Vila Nova e um projeto
                localizado na Macrozona 2, no Sarandi.
              </p>
            </div>
            <div class="construtoraLegenda col-12" id="maiojamaLegenda">
              <h3>MAIOJAMA</h3>
              <p>
                A construtora tem até o momento cinco empreendimentos de médio e
                alto padrão aprovados na capital, quatro estão na Macrozona 1,
                nos bairros Cidade Baixa, Independência e Petrópolis, e um deles
                está localizado na Macrozona 5, no bairro Pedra Redonda.
              </p>
            </div>
            <div class="construtoraLegenda col-12" id="multiplanLegenda">
              <h3>MULTIPLAN</h3>
              <p>
                A construtora tem até o momento dois empreendimentos de alto
                padrão aprovados na capital, ambos na Macrozona 4, no bairro
                Cristal.
              </p>
            </div>
            <div class="construtoraLegenda col-12" id="tendaLegenda">
              <h3>TENDA</h3>
              <p>
                A construtora tem até o momento nove empreendimentos para
                Demanda Habitacional Prioritária aprovados na capital, seis
                deles estão na Macrozona 3, nos bairros Sarandi, Passo das
                Pedras e Rubem Berta. Dois projetos estão localizados na
                Macrozona 8, no bairro Vila Nova e um na Macrozona 5, no bairro
                Cavalhada.
              </p>
            </div>
            <div class="construtoraLegenda col-12" id="mrvLegenda">
              <h3>MRV</h3>
              <p>
                A construtora tem até o momento dois empreendimentos de alto
                padrão aprovados na capital, ambos na Macrozona 4, no bairro
                Cristal.
              </p>
            </div>
            <div class="construtoraLegenda col-12" id="lyxLegenda">
              <h3>LYX</h3>
              <p>
                A construtora tem até o momento nove empreendimentos para
                Demanda Habitacional Prioritária aprovados na capital, quatro
                estão localizados na Macrozona 7, no bairro Restinga, quatro na
                Macrozona 8, nos bairros Ponta Grossa e Vila Nova e um na
                Macrozona 10, na Lomba do Pinheiro.
              </p>
            </div>
          </section>
        </div>
      </div>
      <script
        type="text/javascript"
        src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/js/porto-alegre-regions.js"
      ></script>
      <script type="text/javascript" src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/js/markers.js"></script>
      <script src="<?php echo get_site_url(); ?>/wp-content/themes/jornal-sul21/donos-da-cidade/js/porto-alegre.js"></script>

      <section id="reportagens">
        <div class="min-vw-100 min-vh-100 pb-2 d-flex align-items-center">

          <div class="container text-center text-white">
            <!-- Reportagens -->
            <div class="row g-2 g-lg-3">
              <h1 class="col-12">Reportagens</h1>

              <!-- Matéria 1 -->
              <div class="col-md-6 col-lg-4 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?post_type=especiais&p=220236">
                  <figure>
                    <img class="img-fluid" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/11/donosdacidade-800x311.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Entenda como mapeamos os projetos especiais que mudaram Porto Alegre nos últimos 10 anos 
                    </figcaption>
                  </figure>
                </a>
              </div>

              <!-- Matéria 2 -->
              <div class="col-md-6 col-lg-4 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?post_type=especiais&p=220228">
                  <figure>
                    <img class="img-fluid" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/11/poapmpa-800x311.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Como um restrito grupo de empresários mudou a lógica do planejamento urbano de Porto Alegre 
                    </figcaption>
                  </figure>
                </a>
              </div>

              <!-- Matéria 3 -->
              <div class="col-md-6 col-lg-4 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
               <a href="<?php echo get_site_url(); ?>/?post_type=especiais&p=220204">
                  <figure>
                    <img class="img-fluid" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/11/cidadenilo-800x311.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Exceção da lei: Cidade Nilo e Torres do Beira-Rio indicam que nada é impossível em Porto Alegre  
                    </figcaption>
                  </figure>
                </a>
              </div>

              <!-- Matéria 4 -->
              <div class="col-md-6 col-lg-4 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?post_type=especiais&p=220193">
                  <figure>
                    <img class="img-fluid" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/11/vilakedi-800x311.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Déficit habitacional na Porto Alegre dos grandes empreendimentos
                    </figcaption>
                  </figure>
                </a>
              </div>
              
              <!-- Matéria 5 -->
              <div class="col-md-6 col-lg-4 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?post_type=especiais&p=220185">
                  <figure>
                    <img class="img-fluid" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/11/outdoor-800x311.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Quando a terra vai para a Bolsa de Valores: cidade para quem e para quê?
                    </figcaption>
                  </figure>
                </a>
              </div>
              
              <!-- Matéria 6 -->
              <div class="rascunho col-md-6 col-lg-4 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?post_type=especiais&p=220169">
                  <figure>
                    <img class="img-fluid" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/11/centrohistorico-800x311.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Porto Alegre prepara Plano Diretor ‘bastante liberal’ sob encomenda de empresários
                    </figcaption>
                  </figure>
                </a>
              </div>

          </div>
        </div>
      </section>

      <section id="cases">
        <div class="min-vw-100 min-vh-100 pb-2 d-flex align-items-center">
          <div class="container text-center text-white">
            <!-- Destaques -->
            <div class="row g-2 g-md-4 g-lg-4">
              <h1>Destaques</h1>

              <!-- Destaque 1 -->
              <div class="col-md-6 col-lg-3 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?p=219394&preview=true">
                  
                  <figure>
                    <img class="img-fluid" width="250" height="140" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/11/Screen-Shot-2023-11-03-at-17.31.12-1080x635.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Arena do Grêmio
                    </figcaption>
                  </figure>

                </a>
              </div>

              <!-- Destaque 2 -->
              <div class="col-md-6 col-lg-3 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?p=220020&preview=true">
                  
                  <figure>
                    <img class="img-fluid" width="250" height="140" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/11/Screen-Shot-2023-11-03-at-17.34.14-1080x605.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Botanique
                    </figcaption>
                  </figure>

                </a>
              </div>

              <!-- Destaque 3 -->
              <div class="col-md-6 col-lg-3 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?p=219391&preview=true">
                  
                  <figure>
                    <img class="img-fluid" width="250" height="140" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/06/IMG_9770-1080x720.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Cassol Centerlar  
                    </figcaption>
                  </figure>

                </a>
              </div>

              <!-- Destaque 4 -->
              <div class="col-md-6 col-lg-3 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?p=220023&preview=true">

                  <figure>
                    <img class="img-fluid" width="250" height="140" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/11/Screen-Shot-2023-11-03-at-17.33.27-1080x612.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Central Parque
                    </figcaption>
                  </figure>

                </a>
              </div>
              
              <!-- Destaque 5 -->
              <div class="col-md-6 col-lg-3 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?p=219388&preview=true">
              
                  <figure>
                    <img class="img-fluid" width="250" height="140" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/11/Screen-Shot-2023-11-03-at-17.35.34-1080x611.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Complexo Belvedere 
                    </figcaption>
                  </figure>
              
                </a>
              </div>
              
              <!-- Destaque 6 -->
              <div class="col-md-6 col-lg-3 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?p=219941&preview=true">
              
                  <figure>
                    <img class="img-fluid" width="250" height="140" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/11/Screen-Shot-2023-11-03-at-17.30.15-1080x607.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Edifício Bewiki
                    </figcaption>
                  </figure>
              
                </a>
              </div>
              
              <!-- Destaque 7 -->
              <div class="col-md-6 col-lg-3 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?p=219955&preview=true">
              
                  <figure>
                    <img class="img-fluid" width="250" height="140" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/10/Bele%CC%81m_Novo_Arado.jpeg" alt="">
                    <figcaption class="text-white p-2">
                      Fazenda do Arado
                    </figcaption>
                  </figure>
              
                </a>
              </div>
              
              <!-- Destaque 8 -->
              <div class="col-md-6 col-lg-3 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?p=219958&preview=true">
              
                  <figure>
                    <img class="img-fluid" width="250" height="140" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/06/IMG_9980-1080x720.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Golden Lake
                    </figcaption>
                  </figure>
              
                </a>
              </div>

              <!-- Destaque 9 -->
              <div class="col-md-6 col-lg-3 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?p=219948&preview=true">
              
                  <figure>
                    <img class="img-fluid" width="250" height="140" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/06/IMG_9806-1080x720.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Iguatemi
                    </figcaption>
                  </figure>
              
                </a>
              </div>

              <!-- Destaque 10 -->
              <div class="col-md-6 col-lg-3 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?p=219744&preview=true">
              
                  <figure>
                    <img class="img-fluid" width="250" height="140" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/06/IMG_9958-1080x720.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Pontal
                    </figcaption>
                  </figure>
              
                </a>
              </div>

              <!-- Destaque 11 -->
              <div class="col-md-6 col-lg-3 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?p=219753&preview=true">
              
                  <figure>
                    <img class="img-fluid" width="250" height="140" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/06/IMG_0116-1080x720.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Square Garden 
                    </figcaption>
                  </figure>
              
                </a>
              </div>

              <!-- Destaque 12 -->
              <div class="col-md-6 col-lg-3 d-flex flex-wrap align-items-center justify-content-center align-self-start p-2">
                <a href="<?php echo get_site_url(); ?>/?p=219769&preview=true">
              
                  <figure>
                    <img class="img-fluid" width="250" height="140" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/11/Screen-Shot-2023-11-03-at-17.38.27-1080x607.jpg" alt="">
                    <figcaption class="text-white p-2">
                      Torres do Beira-Rio 
                    </figcaption>
                  </figure>
              
                </a>
              </div>
            
            </div>
            <!-- Destaque FIM -->
          </div>
          <!-- /FIM do contêiner -->
        </div>
        <!-- /FIM da box -->
      </section>
    </main>
    <div class="container">
      <footer id="creditos" class="footer">
          <div class="creditos">
              <h2>Créditos</h2>
          </div>
          <div class="nomes">
              <p><b>Apuração:</b> Lidiane Blanco e Luís Eduardo Gomes</p>
              <p><b>Design web:</b> Carlos Astrada e Matheus Leal</p>
              <p><b>Desenvolvimento:</b> Coletivo Farpa</p>
              <p><b>Direção de Arte:</b> Matheus Leal</p>
              <p><b>Supervisão:</b> Ana Ávila e Luís Eduardo Gomes</p>
              <p><b>Redação Sul21:</b> Ana Ávila, Annie Castro, Duda Romagna, Joana Berwanger, Luciano Velleda, Luís Eduardo Gomes, Luiza Castro, Marihá Maria, Marco Weissheimer, Matheus Leal e Thales Trevisan</p>
          </div>
          <div class="text-center"> <!-- Adicionando classe Bootstrap para centralizar horizontalmente -->
              <a class="farpa" href="https://coletivofarpa.org" target="_blank">
                  <svg width="100" height="100" viewBox="0 0 1673 925" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M0 353.856L110.763 468.516L0 583.176L48.1538 633.027L207.067 468.513L48.1538 304L0 353.856ZM272.413 562.516V633.016H454.013V562.516H272.413Z" fill="white"/>
                      <path d="M515.391 633V392.195H680.333V424.379H570.563V488.747H656.77V520.931H570.563V633H515.391ZM794.261 543.92H877.594L836.215 437.023L794.261 543.92ZM875.87 392.195L963.801 633H908.629L888.514 576.103H783.342L763.801 633H708.629L795.985 392.195H875.87ZM1090.4 493.92C1106.11 493.92 1113.97 488.556 1113.97 477.828V440.471C1113.97 429.743 1106.11 424.379 1090.4 424.379H1046.15V493.92H1090.4ZM1102.47 526.103L1181.78 633H1123.74L1046.15 528.402V633H990.977V392.195H1101.9C1120.29 392.195 1136 396.027 1149.02 403.69C1162.43 411.352 1169.14 421.889 1169.14 435.299V483C1169.14 496.41 1162.62 506.946 1149.6 514.609C1136.57 522.272 1120.86 526.103 1102.47 526.103ZM1302.32 494.494C1318.02 494.494 1325.88 489.13 1325.88 478.402V440.471C1325.88 429.743 1318.02 424.379 1302.32 424.379H1258.06V494.494H1302.32ZM1202.89 633V392.195H1313.81C1332.2 392.195 1347.91 396.027 1360.94 403.69C1374.35 411.352 1381.05 421.889 1381.05 435.299V483.575C1381.05 496.985 1374.35 507.521 1360.94 515.184C1347.91 522.847 1332.2 526.678 1313.81 526.678H1258.06V633H1202.89ZM1494.46 543.92H1577.79L1536.41 437.023L1494.46 543.92ZM1576.07 392.195L1664 633H1608.82L1588.71 576.103H1483.54L1464 633H1408.82L1496.18 392.195H1576.07Z" fill="white"/>
                  </svg>
              </a>
          </div>
      </footer>
  </div>

    <!-- Adicionado JS Popper e Bootstrap -->
    <script
      src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
      integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
      integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
      crossorigin="anonymous"
    ></script>
    <!-- MDB -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.4.1/mdb.min.js"></script>
    
    <!-- Menu mobile Hamburguer offcanvas -->
    <script>
      const menuIcon = document.getElementById('menu-icon');
      const offcanvasNavbar = new bootstrap.Offcanvas(document.getElementById('offcanvasNavbar'));

      menuIcon.addEventListener('click', function () {
        offcanvasNavbar.toggle();
      });
      function closeOffcanvas() {
        offcanvasNavbar.hide(); // Fecha o offcanvas
      }
    </script>
  </body>
</html>